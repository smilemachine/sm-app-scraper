/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
import VueResource from 'vue-resource'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('alert-index', require('./components/AlertIndex.vue'));
Vue.component('application-index', require('./components/ApplicationIndex.vue'));
Vue.component('developer-index', require('./components/DeveloperIndex.vue'));

Vue.use(VueResource)
Vue.http.options.root = window.Laravel.baseUrl;
Vue.http.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
Vue.http.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const app = new Vue({
    el: '#app'
});
