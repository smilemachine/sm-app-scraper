@extends('layouts.main')

@section('content')

<div id="developer">
    <div class="pull-right">
        @if ($developer->blacklisted())
            <a href="{{ route('developer.blacklist.remove', ['id'=>$developer->id]) }}"
                class="btn btn-default">
                <span class="fa fa-minus-circle"></span> Blacklist
            </a>
        @else
            <a href="{{ route('developer.blacklist.add', ['id'=>$developer->id]) }}"
                class="btn btn-default">
                <span class="fa fa-plus-circle"></span> Blacklist
            </a>
        @endif
        @if ($developer->trashed())
            <a href="{{ route('developer.restore', ['id'=>$developer->id]) }}"
                class="btn btn-default">
                Restore
            </a>
        @else
            <a href="{{ route('developer.delete', ['id'=>$developer->id]) }}"
                class="btn btn-danger">
                Delete
            </a>
        @endif
    </div>
    <h1>{{ $developer->name }}</h1>
    <hr/>
    @if ($developer->trashed())
        <div class="alert alert-danger">
            Please note that this developer is currently deleted as of {{ $developer->deleted_at->format('Y-m-d') }}.
        </div>
    @endif
    @if ($developer->blacklisted())
        <div class="alert alert-danger">
            Please note that this developer is blacklisted as of {{ $developer->blacklist->created_at->format('Y-m-d') }}
        </div>
    @endif

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#details" aria-controls="details" role="tab" data-toggle="tab">
                Details
            </a>
        </li>
        <li role="presentation">
            <a href="#applications" aria-controls="applications" role="tab" data-toggle="tab">
                Applications
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="details">
            <div class="form form-horizontal">
                <div class="row">
                    <label class="control-label col-sm-3">Name</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $developer->name }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Store</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $developer->store ? $developer->store->name : null }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">URL</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($developer->url)
                                <a href="{{ $developer->url }}" target="_blank">
                                    {{ $developer->url }}
                                </a>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Email</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($developer->email)
                                <a href="mailto:{{ $developer->email }}" target="_blank">
                                    {{ $developer->email }}
                                </a>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Refreshed At</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $developer->updated_at->format('Y-m-d') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="applications">
            <application-index :search="{{ $search }}"></application-index>
        </div>
    </div>
</div>

@endsection
