@extends('layouts.main')

@section('content')

<div id="developers">
  <h1>Developers</h1>
  <developer-index></developer-index>
</div>

@endsection
