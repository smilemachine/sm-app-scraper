@extends('layouts.main')

@section('content')

<div id="alerts">
  <h1>Alerts</h1>
  <hr/>
  <alert-index></alert-index>
</div>

@endsection
