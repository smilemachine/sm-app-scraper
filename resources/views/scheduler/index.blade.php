@extends('layouts.main')

@section('content')

<div id="scheduler">
    <h1>Scheduler</h1>
    <hr/>
    <div class="alert alert-info">
      <p>This form is split into two parts, both of which will run when submitted.</p>
      <p><strong>Existing Applications</strong> adds x number of Applications that already exist in the database to the queue for updating.</p>
      <p><strong>New Applications</strong> schedules the the IDs listed in the textarea from the given store to be added to the database.</p>
    </div>
    {!!
        Form::open([
            'method' => 'POST',
            'route' => ['scheduler.schedule'],
            'class' => 'form'
        ])
    !!}
        <div class="row">
            <div class="col-sm-6">
                <h3>Existing Applications</h3>
                <div class="form-group">
                    <select name="existing[store]" class="form-control">
                        @if (count($stores) > 1)
                            <option value="">All Stores</option>
                        @endif
                        @foreach ($stores as $store)
                            <option value="{{ $store['id'] }}">{{ $store['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select name="existing[category]" class="form-control">
                        <option value="">All Categories</option>
                        @foreach ($stores as $store)
                            <optgroup label="{{ $store['name'] }}">
                            @foreach ($store['categories'] as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select name="existing[country]" class="form-control">
                        <option value="">All Countries</option>
                        @foreach ($countries as $id => $name)
                            <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select name="existing[priority]" class="form-control">
                        <option value="1">Apps with priority</option>
                        <option value="0">Apps without priority</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="existing[orderBy]"
                        placeholder="Order by: updated_at" value="" class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" name="existing[orderByDirection]"
                        placeholder="Order direction: DESC" value="" class="form-control">
                </div>
                <div class="form-group">
                    <input type="number" name="existing[limit]" placeholder="Limit: 1-5000"
                        min="0" max="5000" value="" class="form-control">
                </div>
            </div>
            <div class="col-sm-6">
                <h3>New Applications</h3>
                <div class="form-group">
                    <select name="new[store]" class="form-control">
                        @if (count($stores) > 1)
                            <option value="">All Stores</option>
                        @endif
                        @foreach ($stores as $store)
                            <option value="{{ $store['id'] }}">{{ $store['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="new[ids]" placeholder="Comma separated list of Application IDs."
                        class="form-control" rows="11"></textarea>
                    <p class="help-block">Eg: <strong>284882215</strong> from  https://itunes.apple.com/us/app/facebook/id<strong>284882215</strong>?mt=8</p>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button class="btn btn-primary">Submit</button>
        </div>
    {!! Form::close() !!}
</div>

@endsection
