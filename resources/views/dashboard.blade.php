@extends('layouts.main')

@section('content')

<h1>Dashboard</h1>

<hr/>

<div class="row">
  <div class="col-sm-12">
      <div class="col-xs-6 col-sm-3 text-center">
        <strong>Total Apps</strong>
        <h2>{{ $appsTotal }}</h2>
      </div>
      <div class="col-xs-6 col-sm-3 text-center">
        <strong>Scraped</strong>
        <h2>{{ $appsScraped }}</h2>
      </div>
      <div class="col-xs-6 col-sm-3 text-center">
        <strong>Unscraped</strong>
        <h2>{{ $appsNotScraped }}</h2>
      </div>
      <div class="col-xs-6 col-sm-3 text-center">
        <strong>Recently Updated</strong>
        <h2>{{ count($appsRecent) }}</h2>
      </div>
  </div>
</div>

@endsection
