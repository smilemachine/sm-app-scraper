@component('application.show-country-tabs', [
    'type' => 'updates',
    'items' => $items,
])
@endcomponent
<div class="tab-content">
    @foreach ($items as $key => $item)
        <div role="tabpanel" class="tab-pane {{ $key == 0 ? 'active' : null }}"
            id="updates-{{ $item['key'] }}">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Version</th>
                        <th>Released</th>
                        <th>Logged</th>
                        <th>Refreshed</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($item['updates']) > 0)
                        @foreach ($item['updates'] as $item)
                            <tr>
                                <td>{{ $item->country->name }}</td>
                                <td>{{ $item->version }}</td>
                                <td>{{ $item->released_at->format('Y-m-d') }}</td>
                                <td>{{ $item->created_at->format('Y-m-d') }}</td>
                                <td>{{ $item->updated_at->format('Y-m-d') }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                No updates found.
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    @endforeach
</div>
