@component('application.show-country-tabs', [
    'type' => 'rankings',
    'items' => $items,
])
@endcomponent
<div class="tab-content">
    @foreach ($items as $key => $item)
        <div role="tabpanel" class="tab-pane {{ $key == 0 ? 'active' : null }}"
            id="rankings-{{ $item['key'] }}">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Category</th>
                        <th>Ranking</th>
                        <th>Logged</th>
                        <th>Refreshed</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($item['rankings']) > 0)
                        @foreach ($item['rankings'] as $item)
                            <tr>
                                <td>{{ $item->country->name }}</td>
                                <td>{{ $item->category->name }}</td>
                                <td>{{ $item->ranking }}</td>
                                <td>{{ $item->created_at->format('Y-m-d') }}</td>
                                <td>{{ $item->updated_at->format('Y-m-d') }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                No rankings found.
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    @endforeach
</div>
