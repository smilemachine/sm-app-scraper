@component('application.show-country-tabs', [
    'type' => 'reviews',
    'items' => $items,
])
@endcomponent
<div class="tab-content">
    @foreach ($items as $key => $item)
        <div role="tabpanel" class="tab-pane {{ $key == 0 ? 'active' : null }}"
            id="reviews-{{ $item['key'] }}">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Total</th>
                        <th>Current</th>
                        <th>Logged</th>
                        <th>Refreshed</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($item['reviews']) > 0)
                        @foreach ($item['reviews'] as $item)
                            <tr>
                                <td>{{ $item->country->name }}</td>
                                <td>{{ $item->total }}</td>
                                <td>{{ $item->current }}</td>
                                <td>{{ $item->created_at->format('Y-m-d') }}</td>
                                <td>{{ $item->updated_at->format('Y-m-d') }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                No reviews found.
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    @endforeach
</div>
