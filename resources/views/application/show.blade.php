@extends('layouts.main')

@section('content')

{!!
    Form::model($application, [
        'method' => 'PATCH',
        'route' => ['application.update', $application->id],
        'class' => 'form form-horizontal'
    ])
!!}
    <div id="application">
        <div class="pull-right">
            @if ($application->blacklisted())
                <a href="{{ route('application.blacklist.remove', ['id'=>$application->id]) }}"
                    class="btn btn-default">
                    <span class="fa fa-minus-circle"></span> Blacklist
                </a>
            @else
                <a href="{{ route('application.blacklist.add', ['id'=>$application->id]) }}"
                    class="btn btn-default">
                    <span class="fa fa-plus-circle"></span> Blacklist
                </a>
            @endif
            @if ($application->trashed())
                <a href="{{ route('application.restore', ['id'=>$application->id]) }}"
                    class="btn btn-default">
                    Restore
                </a>
            @else
                <a href="{{ route('application.delete', ['id'=>$application->id]) }}"
                    class="btn btn-danger">
                    Delete
                </a>
            @endif
            <button class="btn btn-primary">Save</button>
        </div>
        <h1>{{ $application->name }}</h1>
        <hr/>
        @if ($application->trashed())
            <div class="alert alert-danger">
                Please note that this application is currently deleted as of {{ $application->deleted_at->format('Y-m-d') }}.
            </div>
        @endif
        @if ($application->blacklisted())
            <div class="alert alert-danger">
                Please note that this application is blacklisted as of {{ $application->blacklist->created_at->format('Y-m-d') }}
            </div>
        @endif

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#details" aria-controls="details" role="tab" data-toggle="tab">
                    Details
                </a>
            </li>
            <li role="presentation">
                <a href="#rankings" aria-controls="rankings" role="tab" data-toggle="tab">
                    Rankings
                </a>
            </li>
            <li role="presentation">
                <a href="#ratings" aria-controls="ratings" role="tab" data-toggle="tab">
                    Ratings
                </a>
            </li>
            <li role="presentation">
                <a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">
                    Reviews
                </a>
            </li>
            <li role="presentation">
                <a href="#updates" aria-controls="updates" role="tab" data-toggle="tab">
                    Updates
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="details">
                <div class="row">
                    <label class="control-label col-sm-3">Assign Priority</label>
                    <div class="controls col-sm-9">
                      {!!
                          Form::select('priority', $priorities, $application->priority, [
                              'class' => 'form-control'
                          ])
                      !!}
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Name</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $application->name }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Store</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $application->store ? $application->store->name : null }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">URL</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            <a href="{{ $application->url }}" target="_blank">
                                {{ $application->url }}
                            </a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Version</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($latestUpdate)
                                {{ $latestUpdate->version }}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Developer</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($application->developer)
                                <a href="{{ route('developer.show', ['id'=>$application->developer_id]) }}">
                                    {{ $application->developer->name }}
                                </a>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Price</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $application->price }} {{ $application->currency }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Categories</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $categories }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Languages</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $languages }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Installs</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $application->installs_min }} - {{ $application->installs_max }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Content Rating</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $application->content_rating }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Released At</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($application->released_at)
                                {{ $application->released_at->format('Y-m-d') }}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Updated At</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($latestUpdate)
                                {{ $latestUpdate->released_at->format('Y-m-d') }}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Refreshed At</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {{ $application->updated_at->format('Y-m-d') }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Description</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            {!! nl2br($application->description) !!}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-sm-3">Latest Changes</label>
                    <div class="col-sm-9">
                        <p class="form-control-static">
                            @if ($latestUpdate)
                                {!! nl2br($latestUpdate->changes) !!}
                            @endif
                        </p>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="rankings">
                @component('application.show-country-rankings', [
                    'items' => $applicationCountryData
                ])
                @endcomponent
            </div>
            <div role="tabpanel" class="tab-pane" id="ratings">
                @component('application.show-country-ratings', [
                    'items' => $applicationCountryData
                ])
                @endcomponent
            </div>
            <div role="tabpanel" class="tab-pane" id="reviews">
                @component('application.show-country-reviews', [
                    'items' => $applicationCountryData
                ])
                @endcomponent
            </div>
            <div role="tabpanel" class="tab-pane" id="updates">
                @component('application.show-country-updates', [
                    'items' => $applicationCountryData
                ])
                @endcomponent
            </div>
        </div>
    </div>
{!! Form::close() !!}

@endsection
