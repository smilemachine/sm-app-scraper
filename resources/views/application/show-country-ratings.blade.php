@component('application.show-country-tabs', [
    'type' => 'ratings',
    'items' => $items,
])
@endcomponent
<div class="tab-content">
    @foreach ($items as $key => $item)
        <div role="tabpanel" class="tab-pane {{ $key == 0 ? 'active' : null }}"
            id="ratings-{{ $item['key'] }}">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Total</th>
                        <th>Current</th>
                        <th>Logged</th>
                        <th>Refreshed</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($item['ratings']) > 0)
                        @foreach ($item['ratings'] as $item)
                            <tr>
                                <td>{{ $item->country->name }}</td>
                                <td>{{ $item->total }}</td>
                                <td>{{ $item->current }}</td>
                                <td>{{ $item->created_at->format('Y-m-d') }}</td>
                                <td>{{ $item->updated_at->format('Y-m-d') }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                No ratings found.
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    @endforeach
</div>
