@extends('layouts.main')

@section('content')

<div id="applications">
  <h1>Applications</h1>
  <hr/>
  <application-index></application-index>
</div>

@endsection
