<ul class="nav nav-tabs" role="tablist">
    @foreach ($items as $key => $item)
        <li role="presentation" class="{{ $key == 0 ? 'active' : null }}">
            <a href="#{{ $type }}-{{ $item['key'] }}" role="tab"
                aria-controls="{{ $type }}-{{ $item['key'] }}" data-toggle="tab">
                {{ $item['key'] }}
            </a>
        </li>
    @endforeach
</ul>
