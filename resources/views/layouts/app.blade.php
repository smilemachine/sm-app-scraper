<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
      window.Laravel = {
        csrfToken: '{{ csrf_token() }}',
        baseUrl: '{{ url("/") }}'
      };
    </script>
  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Scraper</a>
          </div>
        </div>
      </nav>
      <div class="container">
          @if (session('notification'))
              <div class="alert alert-success">
                  {{ session('notification') }}
              </div>
          @endif
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  @foreach ($errors->all() as $error)
                      <p>{{ $error }}</p>
                  @endforeach
              </div>
          @endif
          @yield('content')
      </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
  </body>
</html>
