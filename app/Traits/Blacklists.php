<?php

namespace App\Traits;

use App\Models\Blacklist;
use App\Scopes\BlacklistingScope;
use Illuminate\Database\Eloquent\Model;

trait Blacklists
{
    /**
     * Boot the blacklisting trait for a model.
     *
     * @return void
     */
    public static function bootBlacklists()
    {
        static::addGlobalScope(new BlacklistingScope);
    }

    /**
     * Determine if the model instance has been blacklisted.
     *
     * @return bool
     */
    public function blacklisted()
    {
        return $this->blacklist ? true : false;
    }

    /**
     * Blacklists an item or a collection of items
     *
     * @return void
     */
    public function addBlacklist()
    {
        if (!$this->blacklist) {
            $blacklist = new Blacklist;
            $blacklist->blacklistable_id = $this->id;
            $blacklist->blacklistable_type = get_class($this);
            $blacklist->save();
        }
    }

    /**
     * Blacklists an item or a collection of items
     *
     * @return void
     */
    public function removeBlacklist()
    {
        if ($this->blacklist) {
            $this->blacklist->delete();
        }
    }
}
