<?php

namespace App\Models;

use App\Events\ApplicationUpdateCreated;
use App\Models\Application;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class ApplicationUpdate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'country_id', 'version', 'changes', 'released_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['released_at', 'created_at', 'updated_at'];

    /**
     * Relationship to a single Application
     */
    public function application()
    {
       return $this->belongsTo(Application::class);
    }

    /**
     * Relationship to a single Country
     */
    public function country()
    {
       return $this->belongsTo(Country::class);
    }

    /**
     * Boot functions
     */
    protected static function boot()
    {
        parent::boot();

        self::created(function (ApplicationUpdate $applicationUpdate) {
            event(new ApplicationUpdateCreated($applicationUpdate));
        });
    }
}
