<?php

namespace App\Models;

use App\Models\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationTag extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'version', 'changes'
    ];

    /**
     * Relationship to a single Application
     */
    public function application()
    {
       return $this->belongsTo(Application::class);
    }
}
