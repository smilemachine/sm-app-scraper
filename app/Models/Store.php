<?php

namespace App\Models;

use App\Models\Application;
use App\Models\Category;
use App\Models\Collections;
use App\Models\Developer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;

    const APPLE = 'apple';
    const GOOGLE = 'google';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'name',
    ];

    /**
     * Relationship to many Applications
     */
    public function applications()
    {
        return $this->hasManyThrough(Application::class, Developer::class);
    }

    /**
     * Relationship to many Categories
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    /**
     * Relationship to many Collections
     */
    public function collections()
    {
        return $this->hasMany(Collection::class);
    }

    /**
     * Relationship to many Developers
     */
    public function developers()
    {
        return $this->hasMany(Developer::class);
    }

    /**
     * Returns a store/category list
     *
     * @return Collection
     */
    public static function getStoreCategoryList()
    {
        return Store::where('key', self::APPLE)
            ->get()
            ->map(function ($store) {
                $categories = $store->categories()
                    ->orderBy('name')
                    ->pluck('name', 'id')
                    ->toArray();

                return [
                    'id' => $store->id,
                    'name' => $store->name,
                    'categories' => $categories,
                ];
            });
    }
}
