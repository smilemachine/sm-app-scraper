<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    /**
     * Get all of the owning Blacklist models
     */
    public function blacklistable()
    {
        return $this->morphTo();
    }
}
