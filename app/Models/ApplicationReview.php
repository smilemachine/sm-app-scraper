<?php

namespace App\Models;

use App\Models\Application;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class ApplicationReview extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'country_id', 'total', 'current'
    ];

    /**
     * Relationship to a single Application
     */
    public function application()
    {
       return $this->belongsTo(Application::class);
    }

    /**
     * Relationship to a single Country
     */
    public function country()
    {
       return $this->belongsTo(Country::class);
    }
}
