<?php

namespace App\Models;

use App\Models\ApplicationCategory;
use App\Models\ApplicationLanguage;
use App\Models\ApplicationRating;
use App\Models\ApplicationRanking;
use App\Models\ApplicationReview;
use App\Models\ApplicationUpdate;
use App\Models\Blacklist;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Developer;
use App\Models\Store;
use App\Models\Tag;
use App\Traits\Blacklists;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes, Blacklists;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id', 'app_id', 'app_key', 'name', 'url', 'version', 'developer_id', 'description', 'price', 'currency', 'installs_min', 'installs_max', 'priority', 'released_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['released_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Relationship to many Categories
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'application_categories');
    }

    /**
     * Relationship to many Categories
     */
    public function applicationCategories()
    {
        return $this->hasMany(ApplicationCategory::class);
    }

    /**
     * Relationship to many ApplicationLanguages
     */
    public function applicationLanguages()
    {
        return $this->hasMany(ApplicationLanguage::class);
    }

    /**
     * Relationship to many ApplicationRankings
     */
    public function applicationRankings()
    {
        return $this->hasMany(ApplicationRanking::class);
    }

    /**
     * Relationship to many ApplicationRatings
     */
    public function applicationRatings()
    {
        return $this->hasMany(ApplicationRating::class);
    }

    /**
     * Relationship to many ApplicationReviews
     */
    public function applicationReviews()
    {
        return $this->hasMany(ApplicationReview::class);
    }

    /**
     * Relationship to many Tags
     */
    public function applicationTags()
    {
        return $this->hasMany(ApplicationTag::class);
    }

    /**
     * Relationship to many Updates
     */
    public function applicationUpdates()
    {
        return $this->hasMany(ApplicationUpdate::class);
    }

    /**
     * Relationship to many Collections
     */
    public function collections()
    {
        return $this->belongsToMany(Category::class, 'application_collections');
    }

    /**
     * Relationship to a single Developer
     */
    public function developer()
    {
       return $this->belongsTo(Developer::class);
    }

    /**
     * Relationship to a single Store
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     * Relationship to many Tags
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'application_tags');
    }

    /**
  	 *	Relationship to a single Blacklist
  	 */
  	public function blacklist()
  	{
    		return $this->morphOne(Blacklist::class, 'blacklistable');
  	}

    /**
     * Dynamic scope to determine whether or not the app has been scraped
     *
     * @param Builder $query
     * @param boolean $scraped
     * @return Builder
     */
    public function scopeScraped($query, $scraped = true)
    {
        if ($scraped === true) {
            return $query->whereNotNull('description');
        }

        return $query->whereNull('description');
    }

    /**
     * Scope to limit the apps that have been updated within the past x weeks
     *
     * @param Builder $query
     * @param integer $weeks
     * @return Builder
     */
    public function scopeRecent($query, $weeks = 4)
    {
        return $query->whereHas('applicationUpdates', function ($q) use ($weeks) {
            $date = Carbon::now()->subWeeks(intval($weeks));
            $q->where('application_updates.created_at', '>=', $date);
        });
    }

    /**
     * Scope to limit the apps to the given priority
     *
     * @param Builder $query
     * @param integer $priority
     * @return Builder
     */
    public function scopePriority($query, $priority = 1)
    {
        if (intval($priority) == 0 || is_null($priority)) {
            return $query->whereNull('priority');
        }

        return $query->where('priority', '=', intval($priority));
    }

    /**
     * Boot functions
     */
    protected static function boot()
    {
        parent::boot();

        self::saving(function (Application $application) {
            if (intval($application->priority) == 0) {
                $application->priority = null;
            }
        });
    }

    /**
     * List of Priorities
     *
     * @return array
     */
    public static function priorities()
    {
        return [
            1 => 'Highest',
            2 => 'Mid',
            3 => 'Poor',
            4 => 'Lowest',
            0 => 'None'
        ];
    }

    /**
     * Percentage of priorities
     *
     * @return array
     */
    public static function priorityPercentage()
    {
        return [
            1 => 40,
            2 => 30,
            3 => 20,
            4 => 10
        ];
    }
}
