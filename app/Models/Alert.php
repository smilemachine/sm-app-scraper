<?php

namespace App\Models;

use App\Models\AlertType;
use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'alert_type_id', 'application_id', 'title', 'description', 'is_read', 'is_actioned'
    ];

    /**
     * Relationship to a single AlertType
     */
    public function alertType()
    {
       return $this->belongsTo(AlertType::class);
    }

    /**
     * Dynamic scope to determine whether or not the app has been scraped
     *
     * @param Builder $query
     * @param string $type
     * @return Builder
     */
    public function scopeAlertType($query, $type = null)
    {
        if ($type) {
            return $query->whereHas('alertType', function ($query) use ($type) {
                $query->where('alertType.key', '=', $type);
            });
        }

        return $query;
    }

    /**
     * Mark as read
     *
     * @return void
     */
    public function markAsRead()
    {
        $this->markReadStatus(true);
    }

    /**
     * Mark as unread
     *
     * @return void
     */
    public function markAsUnread()
    {
        $this->markReadStatus(false);
    }

    /**
     * Change the is_read status
     *
     * @param bool $isRead
     * @return void
     */
    private function markReadStatus($isRead = true)
    {
        $this->is_read = $isRead ? 1 : 0;
        $this->save();
    }

    /**
     * Mark as actioned
     *
     * @return void
     */
    public function markAsActioned()
    {
        $this->markActionedStatus(true);
    }

    /**
     * Mark as unactioned
     *
     * @return void
     */
    public function markAsUnactioned()
    {
        $this->markActionedStatus(false);
    }

    /**
     * Change the is_actioned status
     *
     * @param bool $isActioned
     * @return void
     */
    private function markActionedStatus($isActioned = true)
    {
        $this->is_actioned = $isActioned ? 1 : 0;
        $this->save();
    }
}
