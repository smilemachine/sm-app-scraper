<?php

namespace App\Models;

use App\Models\Application;
use App\Models\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id', 'key', 'name',
    ];

    /**
     * Relationship to many Applications
     */
    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    /**
     * Relationship to one Store
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
