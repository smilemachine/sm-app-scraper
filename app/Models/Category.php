<?php

namespace App\Models;

use App\Models\Application;
use App\Models\Blacklist;
use App\Models\Store;
use App\Traits\Blacklists;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes, Blacklists;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_id', 'key', 'name',
    ];

    /**
     * Relationship to many Applications
     */
    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    /**
     * Relationship to one Store
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
  	 *	Relationship to a single Blacklist
  	 */
  	public function blacklist()
  	{
    		return $this->morphOne(Blacklist::class, 'blacklistable');
  	}
}
