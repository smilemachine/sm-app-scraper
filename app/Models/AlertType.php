<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlertType extends Model
{
    const RATING_DROP = 'rating_drop';
    const RANKING_DROP = 'ranking_drop';
    const NEW_BAD_REVIEW = 'new_bad_review';
    const NEW_UPDATE = 'new_update';
}
