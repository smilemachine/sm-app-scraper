<?php

namespace App\Models;

use App\Models\Application;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class ApplicationCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'category_id'
    ];

    public $timestamps = false;

    /**
     * Relationship to a single Application
     */
    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    /**
     * Relationship to a single Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
