<?php

namespace App\Models;

use App\Events\ApplicationRankingCreated;
use App\Models\Application;
use App\Models\Category;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class ApplicationRanking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'country_id', 'category_id', 'ranking'
    ];

    /**
     * Relationship to a single Application
     */
    public function application()
    {
       return $this->belongsTo(Application::class);
    }

    /**
     * Relationship to a single Category
     */
    public function category()
    {
       return $this->belongsTo(Category::class);
    }

    /**
     * Relationship to a single Country
     */
    public function country()
    {
       return $this->belongsTo(Country::class);
    }

    /**
     * Boot functions
     */
    protected static function boot()
    {
        parent::boot();

        self::created(function (ApplicationRanking $applicationRanking) {
            event(new ApplicationRankingCreated($applicationRanking));
        });
    }
}
