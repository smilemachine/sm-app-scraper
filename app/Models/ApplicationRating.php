<?php

namespace App\Models;

use App\Events\ApplicationRatingCreated;
use App\Models\Application;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class ApplicationRating extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'country_id', 'total', 'current', '1star', '2star', '3star', '4star', '5star'
    ];

    /**
     * Relationship to a single Application
     */
    public function application()
    {
       return $this->belongsTo(Application::class);
    }

    /**
     * Relationship to a single Country
     */
    public function country()
    {
       return $this->belongsTo(Country::class);
    }

    /**
     * Boot functions
     */
    protected static function boot()
    {
        parent::boot();

        self::created(function (ApplicationRating $applicationRating) {
            event(new ApplicationRatingCreated($applicationRating));
        });
    }
}
