<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'name',
    ];

    /**
     * Scope to provide the default Country
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDefault($query)
    {
        return $query->where('key', '=', 'gb');
    }
}
