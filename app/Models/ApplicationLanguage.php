<?php

namespace App\Models;

use App\Models\Application;
use Illuminate\Database\Eloquent\Model;

class ApplicationLanguage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id', 'language'
    ];

    public $timestamps = false;

    /**
     * Relationship to a single Application
     */
    public function application()
    {
        return $this->belongsTo(Application::class);
    }
}
