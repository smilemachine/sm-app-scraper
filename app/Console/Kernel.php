<?php

namespace App\Console;

use App\Console\Commands\CreateApplication;
use App\Console\Commands\GenerateStatistics;
use App\Console\Commands\UpdateApplication;
use App\Console\Commands\UpdateApplicationArchives;
use App\Console\Commands\UpdateApplicationsDetails;
use App\Console\Commands\UpdateApplicationsTopList;
use App\Console\Commands\UpdateCategories;
use App\Console\Commands\UpdateCollections;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateApplication::class,
        GenerateStatistics::class,
        UpdateApplication::class,
        UpdateApplicationArchives::class,
        UpdateApplicationsDetails::class,
        UpdateApplicationsTopList::class,
        UpdateCategories::class,
        UpdateCollections::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Categories or Collections may have changed, sync them
        $schedule->command('scraper:update-categories')->monthly();
        $schedule->command('scraper:update-collections')->monthly();

        // Update rankings and get any new applications that may exist
        $schedule->command('scraper:update-applications-top-list')->daily();

        // Refresh applications
        $schedule->command('scraper:update-applications-details --limit=1100 --includeNoPriority')->hourly();

        // Update statistics
        $schedule->command('scraper:generate-statistics')->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
