<?php

namespace App\Console\Commands;

use App\Jobs\UpdateApplication as UpdateApplicationJob;
use App\Models\Application;
use App\Models\Country;
use App\Models\Store;
use App\Services\AppScraper;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:update-application {application}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the details of an Application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->argument('application');
        $countries = Country::all();
        $application = Application::withTrashed()
            ->withBlacklisted()
            ->where('app_id', $key)
            ->orWhere('app_key', $key)
            ->firstOrFail();

        $countries->each(function ($country, $i) use ($application) {
            // Schedule each Application to run once every 3 seconds
            $queue = ($application->store ? $application->store->key : 'default');
            $delay = Carbon::now()
                ->addSeconds(intval($i + 1) * 3);

            $job = (new UpdateApplicationJob($application, $country))
                ->onQueue($queue)
                ->delay($delay);

            dispatch($job);
        });
    }
}
