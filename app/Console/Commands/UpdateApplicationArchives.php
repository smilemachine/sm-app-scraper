<?php

namespace App\Console\Commands;

use App\Jobs\UpdateApplicationArchives as UpdateApplicationArchivesJob;
use App\Models\Category;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateApplicationArchives extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:update-application-archive {categoryKey?} {--letter=}, {--page=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Applications from a the App Store archive.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $store = Store::where('key', 'apple')->firstOrFail();
        $letter = $this->option('letter');
        $page = $this->option('page');

        if ($categoryKey = $this->argument('categoryKey')) {
            $category = Category::where('key', $categoryKey)
                ->where('store_id', $store->id)
                ->firstOrFail();

            $categories = collect([$category]);
        } else {
            $categories = $store->categories;
        }

        $categories->each(function ($category) use ($store, $letter, $page) {
            if (is_null($letter)) {
                $letters = collect(range('A', 'Z'));
                $letters->push('*');
            } else {
                $letters = collect([$letter]);
            }

            $letters->each(function ($letter, $key) use ($store, $category, $page) {
                // Schedule it to run once every 20 seconds
                $delay = Carbon::now()
                    ->addSeconds((intval($page) + $key + 1) * 20);

                $job = (new UpdateApplicationArchivesJob($category, $letter, $page))
                    ->onQueue($store->key)
                    ->delay($delay);

                dispatch($job);
            });
        });
    }
}
