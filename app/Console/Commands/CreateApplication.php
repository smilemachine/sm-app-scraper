<?php

namespace App\Console\Commands;

use App\Models\Application;
use App\Models\Store;
use Illuminate\Console\Command;

class CreateApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:create-application {application} {--store=apple}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates and schedules the update of a new Application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->argument('application');
        $store = Store::where('key', $this->option('store'))->firstOrFail();
        $application = Application::withTrashed()
            ->withBlacklisted()
            ->where('app_id', $key)
            ->orWhere('app_key', $key)
            ->first();

        if (!$application) {
            $application = Application::create([
                'store_id' => $store->id,
                'app_id' => $key,
                'app_key' => $key,
                'name' => 'UNKNOWN',
                'url' => 'UNKNOWN',
            ]);
        }

        $this->call('scraper:update-application', [
            'application' => $application->app_id
        ]);
    }
}
