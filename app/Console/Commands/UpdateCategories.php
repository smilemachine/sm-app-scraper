<?php

namespace App\Console\Commands;

use App\Jobs\UpdateCategories as UpdateCategoriesJob;
use App\Models\Store;
use Illuminate\Console\Command;

class UpdateCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:update-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Categories for all Stores.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Store::all()->each(function ($store) {
            dispatch(new UpdateCategoriesJob($store));
        });
    }
}
