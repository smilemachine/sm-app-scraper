<?php

namespace App\Console\Commands;

use App\Jobs\UpdateApplication as UpdateApplicationJob;
use App\Models\Application;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateApplicationsDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:update-applications-details {--limit=5000} {--store=apple} {--country=} {--category=} {--includeNoPriority} {--orderBy=updated_at} {--orderByDirection=ASC}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update applications details.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $applicationIDs = $this->getApplicationIDs();
        $countryKey = $this->option('country');

        if ($countryKey) {
            $countries = collect([Country::where('key', $countryKey)->firstOrFail()]);
        } else {
            $countries = Country::get();
        }

        Application::whereIn('id', $applicationIDs)
            ->each(function ($application, $i) use ($countries) {
                $countries->each(function ($country, $j) use ($application, $i) {
                    // Schedule each Application to run once every 3 seconds
                    $queue = ($application->store ? $application->store->key : 'default');
                    $delay = Carbon::now()
                        ->addSeconds(intval($i + $j + 1) * 3);

                    $job = (new UpdateApplicationJob($application, $country))
                        ->onQueue($queue)
                        ->delay($delay);

                    dispatch($job);
                });
            });
    }

    /**
     * We need to get a ratio of the priorities. In order to do so
     * we'll be multiplying the limit by the percentage to get
     * the number of Applications required for each priority.
     *
     * If the user is requesting less than the number of priorities,
     * just provide all applications ordered by priority and updated.
     *
     * @return array
     */
    private function getApplicationIDs()
    {
        $priorityIDs = [];
        $priorityPercentageages = Application::priorityPercentage();
        $limit = $this->option('limit');
        $includeNoPriority = $this->option('includeNoPriority');

        if (empty($limit) || $limit > 5000) {
            $limit = 5000;
        }

        if ($limit > count($priorityPercentageages) && $includeNoPriority !== true) {
            foreach ($priorityPercentageages as $priority => $percentage) {
                if ($limit > 0 && $percentage > 0) {
                    $priorityLimit = intval($limit * $percentage);
                    $priorityIDs[] = $this->getApplicationIDsByPriority($priorityLimit, $priority);
                }
            }
        } else {
            $priorityIDs[] = $this->getApplicationIDsByPriority($limit);
        }

        return array_slice(array_flatten($priorityIDs), 0, $limit);
    }

    /**
     * Returns an array containin x number of Application IDs
     * with an optional priority value and Category
     *
     * @param integer $limit
     * @param integer $priority
     * @return array
     */
    private function getApplicationIDsByPriority($limit, $priority = null) {
        $category = $this->option('category');
        $orderBy = $this->option('orderBy');
        $orderByDirection = $this->option('orderByDirection');

        return Application::select(['id'])
            ->when(intval($priority) > 0, function ($query) use ($priority) {
                return $query->priority(intval($priority));
            })
            ->when($category, function ($query) use ($category) {
                return $query->whereHas('categories', function ($q) use ($category){
                    $q->where('key', '=', $category);
                });
            })
            ->orderBy('priority', 'ASC')
            ->orderBy($orderBy, $orderByDirection)
            ->take(intval($limit))
            ->get()
            ->pluck('id')
            ->all();
    }
}
