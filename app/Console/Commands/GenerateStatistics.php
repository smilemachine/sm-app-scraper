<?php

namespace App\Console\Commands;

use App\Models\Application;
use App\Models\Statistics;
use Illuminate\Console\Command;

class GenerateStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:generate-statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a snapshot of current statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $appsTotal = Application::select(['id'])->count();
        $appsScraped = Application::select(['id'])->scraped(true)->count();
        $appsNotScraped = Application::select(['id'])->scraped(false)->count();
        $appsRecent = Application::recent()->get()->count();

        $statistic = Statistics::create([
            'data' => [
                'applications' => [
                    'total' => $appsTotal,
                    'scraped' => $appsScraped,
                    'notScraped' => $appsNotScraped,
                    'recent' => $appsRecent,
                ],
            ],
        ]);
    }
}
