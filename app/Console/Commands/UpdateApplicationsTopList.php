<?php

namespace App\Console\Commands;

use App\Jobs\UpdateApplications as UpdateApplicationsJob;
use App\Models\Application;
use App\Models\Category;
use App\Models\Country;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateApplicationsTopList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:update-applications-top-list {--store=apple} {--country=} {--category=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Applications from the top list.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $store = Store::where('key', $this->option('store'))->firstOrFail();
        $categoryKey = $this->option('category');
        $countryKey = $this->option('country');

        // Get an array of Categories to fetch Applications from
        if (!$categoryKey) {
            $categories = Category::where('store_id', $store->id)->get();
        } else {
            $categories = collect([Category::where('key', $categoryKey)->firstOrFail()]);
        }

        // Get an array of Countries to fetch Applications from
        if (!$countryKey) {
            $countries = Country::get();
        } else {
            $countries = collect([Country::where('key', $countryKey)->firstOrFail()]);
        }

        // Schedule the jobs
        $countries->each(function ($country, $i) use ($categories) {
            $categories->each(function ($category, $j) use ($country, $i) {
                $delay = Carbon::now()
                    ->addSeconds(intval($i + $j + 1) * 3);

                $job = (new UpdateApplicationsJob($country, $category))
                    ->onQueue($category->store->key)
                    ->delay($delay);

                dispatch($job);
            });
        });
    }
}
