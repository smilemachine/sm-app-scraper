<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class BlacklistingScope implements Scope
{
    /**
     * All of the extensions to be added to the builder.
     *
     * @var array
     */
    protected $extensions = ['WithBlacklisted', 'WithoutBlacklisted', 'OnlyBlacklisted'];

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->doesntHave('blacklist');
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param Builder $builder
     * @return void
     */
    public function extend(Builder $builder)
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Add the with-blacklisted extension to the builder.
     *
     * @param Builder $builder
     * @return void
     */
    protected function addWithBlacklisted(Builder $builder)
    {
        $builder->macro('withBlacklisted', function (Builder $builder) {
            return $builder->withoutGlobalScope($this);
        });
    }

    /**
     * Add the without-blacklisted extension to the builder.
     *
     * @param Builder $builder
     * @return void
     */
    protected function addWithoutBlacklisted(Builder $builder)
    {
        $builder->macro('withoutBlacklisted', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->doesntHave('blacklist');

            return $builder;
        });
    }

    /**
     * Add the only-blacklisted extension to the builder.
     *
     * @param Builder $builder
     * @return void
     */
    protected function addOnlyBlacklisted(Builder $builder)
    {
        $builder->macro('onlyBlacklisted', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->has('blacklist');

            return $builder;
        });
    }
}
