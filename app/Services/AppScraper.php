<?php

namespace App\Services;

use App\Models\Application;
use App\Models\ApplicationRating;
use App\Models\ApplicationReview;
use App\Models\ApplicationUpdate;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Country;
use App\Models\Developer;
use App\Models\Store;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class AppScraper
{
    /**
     * @var string
     */
    private $country;

    /**
     * @var Store
     */
    private $store;

    /**
     * @var string
     */
    private $storePrefix;

    /**
     * @var Client
     */
    private $client;

    /**
     * Construct
     *
     * @param Store $store
     * @param string $country
     * @return void
     */
    public function __construct(Store $store, Country $country)
    {
        $this->store = $store;
        $this->storePrefix = $this->store->key;
        $this->country = $country;
    }

    /**
     * Get a list of Categories associated to a store
     *
     * @return Collection
     */
    public function getCategories()
    {
        $request = (new Request('GET', $this->storePrefix . '/categories'))
            ->withHeader('Accept', 'application/json');

        try {
            $response = $this->client()->send($request);
            return collect(json_decode($response->getBody()->getContents()));
        } catch (RequestException $e) {
            logger()->debug('Failed to retrieve Categories from scraper');
            logger()->error($e);
            return collect([]);
        }
    }

    /**
     * Get a list of Collections associated to a store
     *
     * @return Collection
     */
    public function getCollections()
    {
        $request = (new Request('GET', $this->storePrefix . '/collections'))
            ->withHeader('Accept', 'application/json');

        try {
            $response = $this->client()->send($request);
            return collect(json_decode($response->getBody()->getContents()));
        } catch (RequestException $e) {
            logger()->debug('Failed to retrieve Collections from scraper');
            logger()->error($e);
            return collect([]);
        }
    }

    /**
     * Get a list of Apps from the summary pages
     *
     * @param Category $category
     * @param Collection $collection
     * @param integer $start
     *
     * @return Collection
     */
    public function getApplications(Category $category = null, Collection $collection = null, $start = null)
    {
        $url = $this->storePrefix . '/apps';
        $url .= '?' . http_build_query([
            'country' => $this->country->key,
            'category' => ($category ? $category->key : null),
            'collection' => ($collection ? $collection->key : null),
            'start' => (intval($start) != 0 ? intval($start) : null),
            'num' => 200
        ]);

        $request = (new Request('GET', $url))->withHeader('Accept', 'application/json');

        try {
            $response = $this->client()->send($request);
            return collect(json_decode($response->getBody()->getContents()));
        } catch (RequestException $e) {
            logger()->debug('Failed to retrieve Applications from scraper');
            logger()->error($e);
            return collect([]);
        }
    }

    /**
     * Get details of a specific of App
     *
     * @param Application $application
     *
     * @return mixed
     */
    public function getApplication(Application $application)
    {
        $url = $this->storePrefix . '/apps/' . $application->app_id;
        $url .= '?' . http_build_query([
            'country' => $this->country->key
        ]);

        $request = (new Request('GET', $url))->withHeader('Accept', 'application/json');

        try {
            $response = $this->client()->send($request);
            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            logger()->debug('Failed to retrieve Application #' . $application->id . ' from scraper');
            return null;
        }
    }

    /**
     * Get a list of Apps from the archive
     *
     * @param Category $category
     * @param string $letter
     * @param integer $page
     *
     * @return Collection
     */
    public function getArchive(Category $category, $letter = null, $page = null)
    {
        $url = $this->storePrefix . '/archive/' . $category->key;
        $url .= '?' . http_build_query([
            'country' => $this->country->key,
            'letter' => $letter,
            'page' => (intval($page) != 0 ? intval($page) : null)
        ]);

        $request = (new Request('GET', $url))->withHeader('Accept', 'application/json');

        try {
            $response = $this->client()->send($request);
            return collect(json_decode($response->getBody()->getContents()));
        } catch (RequestException $e) {
            logger()->debug('Failed to retrieve Archive from scraper');
            logger()->error($e);
            return collect([]);
        }
    }

    /**
     * Generates a new GuzzleHTTP Client
     *
     * @return Client
     */
    private function client()
    {
        if ($this->client == null) {
            $this->client = new Client([
                'base_uri' => config('services.smilemachine.scraper.url'),
            ]);
        }

        return $this->client;
    }

    /**
     * Fill an Application with details from the scraper
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function fillApplicationFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'fill' . $storeKey . 'ApplicationFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Fill an Application with details from the scraper
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function fillAppleApplicationFromDetails(Application $application, $details)
    {
        if (isset($details->title)) {
            try {
                $application->name = str_limit($details->title, 255);
                $application->url = str_limit($details->playstoreUrl, 255);
                $application->version = str_limit($details->version, 255);
                $application->description = str_limit($details->description, 5000);
                $application->price = (float) number_format($details->price, 2, '.', '');
                $application->currency = str_limit($details->currency, 255);
                $application->content_rating = $details->contentRating;
                $application->released_at = new Carbon($details->released);
            } catch (\Exception $e) {
                logger()->debug('Failed to apply Application details for #' . $application->id . ' from scraper');
                logger()->error($e);
            }
        }

        return $application;
    }

    /**
     * Fill an Application with details from the scraper summary
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function fillApplicationFromSummary(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'fill' . $storeKey . 'ApplicationFromSummary';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Fill an Application with details from the scraper
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function fillAppleApplicationFromSummary(Application $application, $details)
    {
        try {
            if (!$details) {
                return $application;
            }

            if (isset($details->appId)) {
                $application->app_key = $details->appId;
            }

            if (isset($details->title)) {
                $application->name = str_limit($details->title, 255);
            }

            if (isset($details->url)) {
                $application->url = str_limit($details->url, 255);
            }

            if (isset($details->description)) {
                $application->description = str_limit($details->description, 5000);
            }

            if (isset($details->summary) && empty($application->description)) {
                $application->description = str_limit($application->summary, 5000);
            }

            if (isset($details->price)) {
                $application->price = (float) number_format($details->price, 2, '.', '');
            }

            if (isset($details->currency)) {
                $application->currency = str_limit($details->currency, 255);
            }

            return $application;
        } catch (\Exception $e) {
            logger()->debug('Failed to apply Application summary details for #' . $application->id . ' from scraper');
            logger()->error($e);
        }

        return $application;
    }

    /**
     * Assign the Application with Categories provided by the Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function assignApplicationCategoriesFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'assign' . $storeKey . 'ApplicationCategoriesFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Assign the Application with Categories provided by the Apple Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    private function assignAppleApplicationCategoriesFromDetails(Application $application, $details)
    {
        if (isset($details->genreIds) && is_array($details->genreIds)) {
            $categories = Category::withTrashed()
                ->withBlacklisted()
                ->whereIn('key', $details->genreIds)
                ->pluck('id', 'key')
                ->all();

            collect($details->genreIds)
                ->filter(function ($categoryKey) use ($categories) {
                    return isset($categories[$categoryKey]);
                })
                ->map(function ($categoryKey) use ($application, $categories) {
                    return [
                        'application_id' => $application->id,
                        'category_id' => $categories[$categoryKey],
                    ];
                })
                ->each(function ($attributes) use ($application) {
                    $application->applicationCategories()
                        ->updateOrCreate($attributes, $attributes);
                });
        }

        return $application;
    }

    /**
     * Assign the application to a Developer provided by the Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function assignApplicationDeveloperFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'assign' . $storeKey . 'ApplicationDeveloperFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Assign the application to a Developer provided by the Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    private function assignAppleApplicationDeveloperFromDetails(Application $application, $details)
    {
        if (isset($details->developer)) {
            try {
                $developer = Developer::firstOrNew([
                    'store_id' => $this->store->id,
                    'key' => $details->developer->devId,
                ]);

                if (empty($developer->name)) {
                    $developer->name = str_limit($details->developer->devId, 255);
                }

                if (isset($details->developerWebsite)) {
                    $developer->url = str_limit($details->developerWebsite, 255);
                }

                if (isset($details->developerUrl)) {
                    $developer->url = str_limit($details->developerUrl, 255);
                }

                $developer->save();
                $application->developer()->associate($developer);
                $application->save();
            } catch (\Exception $e) {
                logger()->debug('Failed to assign Developer for #' . $application->id . ' from scraper');
                logger()->error($e);
            }
        }

        return $application;
    }

    /**
     * Assign the Application with Languages provided by the Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function assignApplicationLanguagesFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'assign' . $storeKey . 'ApplicationLanguagesFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Assign the Application with Languages provided by the Apple Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    private function assignAppleApplicationLanguagesFromDetails(Application $application, $details)
    {
        if (isset($details->languages) && is_array($details->languages)) {
            collect($details->languages)
                ->map(function ($language) use ($application) {
                    return [
                        'application_id' => $application->id,
                        'language' => $language,
                    ];
                })
                ->each(function ($attributes) use ($application) {
                    $application->applicationLanguages()
                        ->updateOrCreate($attributes, $attributes);
                });
        }

        return $application;
    }

    /**
     * Assign the Application with Ratings provided by the Store
     * If we have a rating, check if there is a difference in ratings
     *  - If not, then just update the updated_at field
     *  - If there is, create a new record and trigger an event
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function assignApplicationRatingsFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'assign' . $storeKey . 'ApplicationRatingsFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Assign the Application with Ratings provided by the Apple Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    private function assignAppleApplicationRatingsFromDetails(Application $application, $details)
    {
        try {
            if (!$details || !isset($details->score)
                || !isset($details->currentVersionScore)) {
                return $application;
            }

            $latestRating = ApplicationRating::where('application_id', $application->id)
                ->where('country_id', $this->country->id)
                ->orderBy('updated_at', 'DESC')
                ->first();

            if ($latestRating && $latestRating->total == $details->score
                && $latestRating->current == $details->currentVersionScore) {
                $latestRating->updated_at = Carbon::now();
                $latestRating->save();
            } else {
                ApplicationRating::create([
                    'application_id' => $application->id,
                    'country_id' => $this->country->id,
                    'total' => $details->score,
                    'current' => $details->currentVersionScore
                ]);
            }

            return $application;
        } catch (\Exception $e) {
            logger()->debug('Failed to apply ApplicationRating for #' . $application->id . ' from scraper');
            logger()->error($e);
        }

        return $application;
    }

    /**
     * Assign the Application with Updates provided by the Store
     * If we have an update, check if there is a newer version
     *  - If not, then just update the updated_at field
     *  - If there is, create a new record and trigger an event
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function assignApplicationUpdatesFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'assign' . $storeKey . 'ApplicationUpdatesFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Assign the Application with Updates provided by the Apple Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    private function assignAppleApplicationUpdatesFromDetails(Application $application, $details)
    {
        try {
            if (!$details || !isset($details->version) || !isset($details->releaseNotes)) {
                return $application;
            }

            $detailsUpdate = Carbon::parse($details->updated);
            $latestUpdate = ApplicationUpdate::where('application_id', $application->id)
                ->where('country_id', $this->country->id)
                ->orderBy('updated_at', 'DESC')
                ->first();

            if ($latestUpdate && $latestUpdate->released_at->format('Ymd') == $detailsUpdate->format('Ymd')) {
                $latestUpdate->updated_at = Carbon::now();
                $latestUpdate->save();
            } else if ($detailsUpdate) {
                ApplicationUpdate::create([
                    'application_id' => $application->id,
                    'country_id' => $this->country->id,
                    'version' => $details->version,
                    'changes' => str_limit($details->releaseNotes, 1000),
                    'released_at' => $detailsUpdate,
                ]);
            }

            return $application;
        } catch (\Exception $e) {
            logger()->debug('Failed to apply ApplicationUpdate for #' . $application->id . ' from scraper');
            logger()->error($e);
        }

        return $application;
    }

    /**
     * Assign the Application with Reviews provided by the Store
     * If we have a review, check if there is a difference in reviews
     *  - If not, then just update the updated_at field
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    public function assignApplicationReviewsFromDetails(Application $application, $details)
    {
        $storeKey = studly_case($this->store->key);
        $function = 'assign' . $storeKey . 'ApplicationReviewsFromDetails';

        if (method_exists(self::class, $function)) {
            return $this->{$function}($application, $details);
        }

        return $application;
    }

    /**
     * Assign the Application with Reviews provided by the Apple Store
     *
     * @param Application $application
     * @param object $details
     * @return Application
     */
    private function assignAppleApplicationReviewsFromDetails(Application $application, $details)
    {
        try {
            if (!$details || !isset($details->currentVersionReviews)) {
                return $application;
            }

            $latestReview = ApplicationReview::where('application_id', $application->id)
                ->where('country_id', $this->country->id)
                ->orderBy('updated_at', 'DESC')
                ->first();

            if ($latestReview && $latestReview->current == $details->currentVersionReviews) {
                $latestReview->updated_at = Carbon::now();
                $latestReview->save();
            } else {
                ApplicationReview::create([
                    'application_id' => $application->id,
                    'country_id' => $this->country->id,
                    'current' => $details->currentVersionReviews
                ]);
            }

            return $application;
        } catch (\Exception $e) {
            logger()->debug('Failed to apply ApplicationReview for #' . $application->id . ' from scraper');
            logger()->error($e);
        }

        return $application;
    }

}
