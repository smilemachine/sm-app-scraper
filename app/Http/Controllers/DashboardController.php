<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Statistics;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $statistics = Statistics::orderBy('id', 'DESC')->first();

        if (!$statistics) {
            $statistics = new Stastics(['data' => json_encode([])]);
            $statistics->save();
        }

        $appsTotal = 0;
        $appsScraped = 0;
        $appsNotScraped = 0;
        $appsRecent = 0;

        if ($statistics->data && isset($statistics->data->applications)) {
            $data = $statistics->data;

            if (isset($data->applications->total)) {
                $appsTotal = $data->applications->total;
            }

            if (isset($data->applications->scraped)) {
                $appsScraped = $data->applications->scraped;
            }

            if (isset($data->applications->notScraped)) {
                $appsNotScraped = $data->applications->notScraped;
            }

            if (isset($data->applications->recent)) {
                $appsRecent = $data->applications->recent;
            }
        }

        return response()->view('dashboard', compact('appsTotal', 'appsScraped', 'appsNotScraped', 'appsRecent'));
    }
}
