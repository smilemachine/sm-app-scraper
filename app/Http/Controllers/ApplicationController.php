<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\ApplicationRating;
use App\Models\ApplicationRanking;
use App\Models\ApplicationReview;
use App\Models\ApplicationUpdate;
use App\Models\Country;
use App\Models\Store;
use App\Http\Requests\UpdateApplicationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ApplicationController extends Controller
{
    /**
     * Returns a list of all the Applications
     *
     * @return Response
     */
    public function index()
    {
        return response()->view('application.index', []);
    }

    /**
     * Shows a single Application
     *
     * @param integer $id
     * @return Response
     */
    public function show($id)
    {
        $application = $this->getModel($id);
        $application->priority = intval($application->priority);
        $countries = Country::orderBy('name', 'ASC')->get();

        $applicationCategories = $application->categories()
            ->orderBy('name')
            ->pluck('name')
            ->all();

        $applicationLanguages = $application->applicationLanguages()
            ->orderBy('language', 'ASC')
            ->pluck('language')
            ->all();

        $applicationCountryData = $countries->map(function ($country) use ($application) {
            $applicationRatings = ApplicationRating::where('application_id', $application->id)
                ->where('country_id', $country->id)
                ->orderBy('updated_at', 'DESC')
                ->get();

            $applicationRankings = ApplicationRanking::where('application_id', $application->id)
                ->where('country_id', $country->id)
                ->orderBy('updated_at', 'DESC')
                ->get();

            $applicationReviews = ApplicationReview::where('application_id', $application->id)
                ->where('country_id', $country->id)
                ->orderBy('updated_at', 'DESC')
                ->get();

            $applicationUpdates = ApplicationUpdate::where('application_id', $application->id)
                ->where('country_id', $country->id)
                ->orderBy('updated_at', 'DESC')
                ->get();

            return [
                'key' => strtoupper($country->key),
                'ratings' => $applicationRatings,
                'rankings' => $applicationRankings,
                'reviews' => $applicationReviews,
                'updates' => $applicationUpdates,
            ];
        });

        $categories = implode(', ', $applicationCategories);
        $languages = implode(', ', $applicationLanguages);
        $priorities = Application::priorities();

        $latestUpdate = ApplicationUpdate::where('application_id', $application->id)
            ->orderBy('updated_at')
            ->first();

        return response()->view('application.show', compact('application', 'categories', 'priorities', 'languages', 'applicationCountryData', 'latestUpdate'));
    }

    /**
     * Updates an existing Application
     *
     * @param integer $id
     * @param UpdateApplicationRequest $request
     * @return Redirect
     */
    public function update($id, UpdateApplicationRequest $request)
    {
        $application = $this->getModel($id);
        $application->fill($request->only(['priority']));
        $application->save();

        return redirect()->route('application.show', ['id' => $id])
            ->with('notification', 'Application has been successfully updated.');
    }

    /**
     * Blacklists an Application
     *
     * @param integer $id
     * @return Redirect
     */
    public function addBlacklist($id)
    {
        $this->getModel($id)->addBlacklist();

        return redirect()->route('application.show', ['id' => $id])
            ->with('notification', 'Application has been successfully blacklisted.');
    }

    /**
     * Removes the Blacklist on an Application
     *
     * @param integer $id
     * @return Redirect
     */
    public function removeBlacklist($id)
    {
        $this->getModel($id)->removeBlacklist();

        return redirect()->route('application.show', ['id' => $id])
            ->with('notification', 'Application has been successfully un-blacklisted.');
    }

    /**
     * Deletes an Application
     *
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
        $this->getModel($id)->delete();

        return redirect()->route('application.show', ['id' => $id])
            ->with('notification', 'Application has been successfully deleted.');
    }

    /**
     * Restores an Application
     *
     * @param integer $id
     * @return Redirect
     */
    public function restore($id)
    {
        $this->getModel($id)->restore();

        return redirect()->route('application.show', ['id' => $id])
            ->with('notification', 'Application has been successfully restored.');
    }

    /**
     * Gets an Application with a specified ID
     *
     * @param integer $id
     * @return Application
     */
    private function getModel($id)
    {
        return Application::where('id', $id)
            ->withTrashed()
            ->withBlacklisted()
            ->firstOrFail();
    }
}
