<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Developer;
use App\Models\Store;
use App\Http\Requests\UpdateDeveloperRequest;
use Illuminate\Http\Request;

class DeveloperController extends Controller
{
    /**
     * Returns a list of all the Developers
     *
     * @return Response
     */
    public function index()
    {
        return response()->view('developer.index');
    }

    /**
     * Shows a single Developer
     *
     * @param integer $id
     * @return Response
     */
    public function show($id)
    {
        $developer = $this->getModel($id);
        $search = json_encode([
            'developer' => $developer->id,
            'blacklisted' => 1,
            'trashed' => 1,
        ]);

        return response()->view('developer.show', compact('developer', 'search'));
    }

    /**
     * Blacklists an Developer
     *
     * @param integer $id
     * @return Redirect
     */
    public function addBlacklist($id)
    {
        $this->getModel($id)->addBlacklist();

        return redirect()->route('developer.show', ['id' => $id])
            ->with('notification', 'Developer has been successfully blacklisted.');
    }

    /**
     * Removes the Blacklist on an Developer
     *
     * @param integer $id
     * @return Redirect
     */
    public function removeBlacklist($id)
    {
        $this->getModel($id)->removeBlacklist();

        return redirect()->route('developer.show', ['id' => $id])
            ->with('notification', 'Developer has been successfully un-blacklisted.');
    }

    /**
     * Deletes an Developer
     *
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
        $this->getModel($id)->delete();

        return redirect()->route('developer.show', ['id' => $id])
            ->with('notification', 'Developer has been successfully deleted.');
    }

    /**
     * Restores an Developer
     *
     * @param integer $id
     * @return Redirect
     */
    public function restore($id)
    {
        $this->getModel($id)->restore();

        return redirect()->route('developer.show', ['id' => $id])
            ->with('notification', 'Developer has been successfully restored.');
    }

    /**
     * Gets an Developer with a specified ID
     *
     * @param integer $id
     * @return Developer
     */
    private function getModel($id)
    {
        return Developer::where('id', $id)
            ->withTrashed()
            ->withBlacklisted()
            ->firstOrFail();
    }
}
