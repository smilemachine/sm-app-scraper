<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Category;
use App\Models\Country;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SchedulerController extends Controller
{
    /**
     * Returns a form to schedule application updates
     *
     * @return Response
     */
    public function index()
    {
        $stores = Store::getStoreCategoryList();
        $countries = Country::pluck('name', 'id')->all();

        return response()->view('scheduler.index', compact('countries', 'stores', 'priorities'));
    }

    /**
     * Schedule applications according to the settings
     *
     * @param Request $request
     * @return Response
     */
    public function schedule(Request $request)
    {
        $this->scheduleExistingApplications($request->get('existing', []));
        $this->scheduleNewApplications($request->get('new', []));

        return redirect()->route('scheduler.index')
            ->with('notification', 'Applications have been scheduled.');
    }

    /**
     * Schedule existing applications to be updated
     *
     * @param array $data
     * @return void
     */
    private function scheduleExistingApplications($data)
    {
        $params = [];
        $limit = 0;

        if (isset($data['store'])) {
            $store = Store::findOrFail($data['store']);
            $params['--store'] = $store->key;
        }

        if (isset($data['category'])) {
            $category = Category::findOrFail($data['category']);
            $params['--category'] = $category->key;
        }

        if (isset($data['country'])) {
            $country = Country::findOrFail($data['country']);
            $params['--country'] = $country->key;
        }

        if (!isset($data['priority']) || intval($data['priority'] == 0)) {
            $params['--includeNoPriority'] = true;
        }

        if (isset($data['orderBy'])) {
            $params['--orderBy'] = $data['orderBy'];
        }

        if (isset($data['orderByDirection'])) {
            $params['--orderByDirection'] = $data['orderByDirection'];
        }

        if (isset($data['limit'])) {
            $limit = intval($data['limit']);
            $params['--limit'] = $limit;
        }

        Artisan::call('scraper:update-applications-details', $params);
    }

    /**
     * Schedule new applications to be updated
     *
     * @param array $data
     * @return void
     */
    private function scheduleNewApplications($data)
    {
        $ids = [];
        $countries = Country::all();

        if (isset($data['store'])) {
            $store = Store::findOrFail($data['store']);
        } else {
            app()->abort(404);
        }

        if (isset($data['ids'])) {
            $ids = array_map('trim', explode(',', $data['ids']));
        }

        collect($ids)->each(function ($id) use ($store) {
            Artisan::call('scraper:create-application', [
                'application' => $id,
                '--store' => $store->key,
            ]);
        });
    }
}
