<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class AlertController extends Controller
{
    /**
     * Returns a list of all the Applications
     *
     * @return Response
     */
    public function index()
    {
        return response()->view('alert.index', []);
    }
}
