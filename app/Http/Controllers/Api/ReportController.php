<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Returns an array for use with the Snapshot Updated-Rating report
     *
     * @param Request $request
     * @return Response
     */
    public function updatedVsRating(Request $request)
    {
        $months12 = Carbon::now()->subMonths(12)->format('Y-m-d');
        $months6 = Carbon::now()->subMonths(6)->format('Y-m-d');
        $months3 = Carbon::now()->subMonths(3)->format('Y-m-d');
        $months1 = Carbon::now()->subMonths(1)->format('Y-m-d');

        $releasedSql = '(CASE
            WHEN `application_updates`.`released_at` >= "' . $months1 . '"
                THEN "0-3"
            WHEN `application_updates`.`released_at` >= "' . $months3 . '"
                AND `application_updates`.`released_at` < "' . $months1 . '"
                THEN "1-3"
            WHEN `application_updates`.`released_at` >= "' . $months6 . '"
                AND `application_updates`.`released_at` < "' . $months3 . '"
                THEN "3-6"
            WHEN `application_updates`.`released_at` >= "' . $months12 . '"
                AND `application_updates`.`released_at` < "' . $months6 . '"
                THEN "6-12"
            ELSE
                "12+"
            END)';

        $report = DB::table('applications')
            ->select([
                DB::raw('AVG(`application_ratings`.`total`) AS `total`'),
                DB::raw('AVG(`application_ratings`.`current`) AS `current`'),
                DB::raw($releasedSql . ' AS `released`'),
            ])
            ->join('application_updates', 'application_updates.application_id', '=', 'applications.id')
            ->join('application_ratings', 'application_ratings.application_id', '=', 'applications.id')
            ->groupBy(DB::raw($releasedSql))
            ->get()
            ->map(function ($application) {
                return [
                    'total' => (float) $application->total,
                    'current' => (float) $application->current,
                    'released' => $application->released,
                ];
            });

        return response()->json($report);
    }
}
