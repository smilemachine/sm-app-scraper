<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AlertIndexRequest;
use App\Http\Requests\Api\AlertActionedRequest;
use App\Models\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlertController extends Controller
{
    /**
     * Returns a Collection of Applications
     *
     * @param ApplicationIndexRequest $request
     * @return Response
     */
    public function index(AlertIndexRequest $request)
    {
        $alertType = $request->get('alertType', null);
        $application = $request->get('application', null);
        $isActioned = $request->get('isActioned', null);
        $created = $request->get('created', null);
        $createdDirection = $request->get('createdDirection');
        $description = $request->get('description', null);
        $application = $request->get('application', null);

        // Get the Alerts
        $alerts = DB::table('alerts')
            ->select([
                'alerts.id',
                DB::raw('`alert_types`.`name` AS `alert_type`'),
                'alerts.application_id',
                DB::raw('`applications`.`name` AS `application`'),
                'alerts.description',
                'alerts.is_actioned',
                DB::raw('DATE_FORMAT(`alerts`.`created_at`, "%Y-%m-%d")
                    AS `created_at`'),
            ])
            ->join('alert_types', 'alert_types.id', '=', 'alerts.alert_type_id')
            ->join('applications', 'applications.id', '=', 'alerts.application_id')
            ->when(!is_null($alertType), function ($query) use ($alertType) {
                return $query->where('alert_type_id', intval($alertType));
            })
            ->when(!is_null($application), function ($query) use ($application) {
                return $query->where('applications.name', 'LIKE', '%' . $application . '%');
            })
            ->when(!is_null($isActioned), function ($query) use ($isActioned) {
                return $query->where('is_actioned', intval($isActioned));
            })
            ->when($created && $createdDirection, function ($query)
                use ($created, $createdDirection) {
                $direction = $createdDirection == 'MORE' ? '>' : '<';
                return $query->where('alerts.created_at', $direction, $created);
            })
            ->when($description, function ($query) use ($description) {
                return $query->where('alerts.description', 'LIKE', '%' . $description . '%');
            })
            ->orderBy('created_at', 'DESC')
            ->orderBy('applications.name', 'ASC')
            ->paginate(50);

        return response()->json($alerts);
    }

    /**
     * Mark the requested items as either Actioned or Unactioned
     *
     * @param AlertActionedRequest $request
     * @return Response
     */
    public function actioned(AlertActionedRequest $request)
    {
        $isActioned = intval($request->get('isActioned'));
        $items = $request->get('items');
        $ids = Alert::whereIn('id', $items)
            ->get()
            ->each(function ($item) use ($isActioned) {
                if ($isActioned) {
                    $item->markAsActioned();
                } else {
                    $item->markAsUnactioned();
                }
            })
            ->pluck('id')
            ->all();

        return response()->json($ids);
    }
}
