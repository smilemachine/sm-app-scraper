<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ApplicationIndexRequest;
use App\Http\Requests\Api\UpdatePriorityRequest;
use App\Models\Application;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{
    /**
     * Returns a Collection of Applications
     *
     * @param ApplicationIndexRequest $request
     * @return Response
     */
    public function index(ApplicationIndexRequest $request)
    {
        $blacklisted = intval($request->get('blacklisted', 0));
        $trashed = intval($request->get('trashed', 0));
        $name = $request->get('name', null);
        $category = intval($request->get('category', 0));
        $rating = $request->get('rating', null);
        $ratingDirection = $request->get('ratingDirection');
        $store = intval($request->get('store', 0));
        $developer = intval($request->get('developer', 0));
        $updated = $request->get('updated', null);
        $updatedDirection = $request->get('updatedDirection');
        $priority = $request->get('priority', null);
        $priorityDirection = $request->get('priorityDirection');
        $country = Country::default()->firstOrFail();

        // Ratings subquery
        $queryRating = DB::table('application_ratings')
            ->select('total')
            ->whereRaw('`application_ratings`.`application_id` = `applications`.`id`')
            ->whereRaw('`application_ratings`.`country_id` = ' . $country->id)
            ->orderBy('application_ratings.updated_at', 'DESC')
            ->take(1)
            ->toSql();

        // Version subquery
        $queryVersion = DB::table('application_updates')
            ->select('version')
            ->whereRaw('`application_updates`.`application_id` = `applications`.`id`')
            ->whereRaw('`application_updates`.`country_id` = ' . $country->id)
            ->orderBy('application_updates.updated_at', 'DESC')
            ->take(1)
            ->toSql();

        // Updated subquery
        $queryUpdated = DB::table('application_updates')
            ->select(DB::raw('DATE_FORMAT(`application_updates`.`released_at`, "%Y-%m-%d")
                AS `released_at`'))
            ->whereRaw('`application_updates`.`application_id` = `applications`.`id`')
            ->whereRaw('`application_updates`.`country_id` = ' . $country->id)
            ->orderBy('application_updates.updated_at', 'DESC')
            ->take(1)
            ->toSql();

        // Get the Applications
        $applications = DB::table('applications')
            ->select([
                'applications.id',
                'applications.name',
                DB::raw('`stores`.`name` AS `store`'),
                DB::raw('(' . $queryRating . ') AS `rating`'),
                DB::raw('(' . $queryVersion . ') AS `version`'),
                'applications.priority',
                DB::raw('(' . $queryUpdated . ') AS `updated_at`'),
                DB::raw('DATE_FORMAT(`applications`.`updated_at`, "%Y-%m-%d")
                    AS `refreshed_at`'),
                'applications.deleted_at',
                DB::raw('`blacklists`.`created_at` AS `blacklisted_at`'),
            ])
            ->leftJoin('stores', 'applications.store_id', '=', 'stores.id')
            ->leftJoin('blacklists', function ($query) {
                $query->on('blacklists.blacklistable_id', '=', 'applications.id')
                    ->where('blacklists.blacklistable_type', '=', Application::class);
            })
            ->when($blacklisted == 0, function ($query) {
                return $query->whereNull('blacklists.id');
            })
            ->when($blacklisted == 2, function ($query) {
                return $query->whereNotNull('blacklists.id');
            })
            ->when($trashed == 0, function ($query) {
                return $query->whereNull('applications.deleted_at');
            })
            ->when($trashed == 2, function ($query) {
                return $query->whereNotNull('applications.deleted_at');
            })
            // ->when($category > 0, function ($query) use ($category) {
            //     return $query->whereHas('categories', function ($q) use ($category) {
            //         $q->where('categories.id', '=', $category);
            //     });
            // })
            ->when($name, function ($query) use ($name) {
                return $query->whereRaw('(
                    `applications`.`name` LIKE "%' . $name . '%"
                    OR
                    `applications`.`description` LIKE "%' . $name . '%"
                )');
            })
            ->when($rating && $ratingDirection, function ($query)
                use ($rating, $ratingDirection) {
                $direction = $ratingDirection == 'MORE' ? '>' : '<';
                return $query->where('rating_calculated_total', $direction, $rating);
            })
            ->when($store > 0, function ($query) use ($store) {
                return $query->where('stores.id', '=', $store);
            })
            ->when($developer > 0, function ($query) use ($developer) {
                return $query->where('developer_id', '=', $developer);
            })
            // ->when($updated && $updatedDirection, function ($query)
            //     use ($updated, $updatedDirection) {
            //     $direction = $updatedDirection == 'MORE' ? '>' : '<';
            //     return $query->where('app_updated_at', $direction, $updated);
            // })
            ->when($priorityDirection, function ($query)
                use ($priority, $priorityDirection) {

                switch ($priorityDirection) {
                    case 'MORE':
                        if ($priority) {
                            $query->where('priority', '>', $priority);
                        }
                        break;
                    case 'LESS':
                        if ($priority) {
                            $query->where('priority', '<', $priority);
                        }
                        break;
                    case 'NONE':
                        $query->whereNull('priority');
                        break;
                    default:
                        break;
                }

                return $query;
            })
            ->orderBy('rating', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->orderBy('applications.name', 'ASC')
            ->paginate(50);

        return response()->json($applications);
    }

    /**
     * Updates the priority of the requested items
     *
     * @param BlacklistAddRequest $request
     * @return Response
     */
    public function updatePriority(UpdatePriorityRequest $request)
    {
        $items = $request->get('items');
        $priority = $request->get('priority');

        if ($priority == 0) {
            $priority = null;
        }

        Application::withBlacklisted()
            ->whereIn('id', $items)
            ->update(['priority' => $priority]);

        return response()->json($items);
    }

    /**
     * Returns a list of available priorities
     *
     * @return Response
     */
    public function priorityList()
    {
        return response()->json(['data' => Application::priorities()]);
    }
}
