<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DeveloperIndexRequest;
use App\Models\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeveloperController extends Controller
{
    /**
     * Returns a Collection of Developers
     *
     * @param DeveloperIndexRequest $request
     * @return Response
     */
    public function index(DeveloperIndexRequest $request)
    {
        $blacklisted = intval($request->get('blacklisted', 0));
        $name = $request->get('name', null);
        $store = intval($request->get('store', 0));

        $developers = Developer::select([
                'developers.id',
                'developers.name',
                DB::raw('`stores`.`name` AS `store`'),
                'developers.url',
                'developers.email',
                DB::raw('DATE_FORMAT(`developers`.`updated_at`, "%Y-%m-%d")
                    AS `refreshed_at`')
            ])
            ->leftJoin('stores', 'developers.store_id', '=', 'stores.id')
            ->when($blacklisted == 1, function ($query) {
                return $query->withBlacklisted();
            })
            ->when($blacklisted == 2, function ($query) {
                return $query->onlyBlacklisted();
            })
            ->when($name, function ($query) use ($name) {
                return $query->whereRaw('(
                    `developers`.`name` LIKE "%' . $name . '%"
                    OR
                    `developers`.`description` LIKE "%' . $name . '%"
                )');
            })
            ->when($store > 0, function ($query) use ($store) {
                return $query->where('stores.id', '=', $store);
            })
            ->paginate(50);

        return response()->json($developers);
    }
}
