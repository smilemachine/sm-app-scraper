<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AlertType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlertTypeController extends Controller
{
    /**
     * Returns a Collection of Applications
     *
     * @return Response
     */
    public function list()
    {
        $alertTypes = AlertType::orderBy('name', 'ASC')
            ->get()
            ->map(function($alertType) {
                return [
                    'id' => $alertType->id,
                    'name' => $alertType->name,
                ];
            });

        return response()->json(['data' => $alertTypes]);
    }
}
