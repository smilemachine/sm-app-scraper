<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\BlacklistAddRequest;
use App\Http\Requests\Api\BlacklistRemoveRequest;
use App\Models\Application;
use App\Models\Category;
use App\Models\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlacklistController extends Controller
{
    /**
     * Adds the requested items to the Blacklist
     *
     * @param BlacklistAddRequest $request
     * @return Response
     */
    public function add(BlacklistAddRequest $request)
    {
        $items = $this->items($request->get('type'), $request->get('items'))
            ->each(function ($item) {
                $item->addBlacklist();
            })
            ->pluck('id')
            ->all();

        return response()->json($items);
    }

    /**
     * Removes the requested items from the Blacklist
     *
     * @param BlacklistRemoveRequest $request
     * @return Response
     */
    public function remove(BlacklistRemoveRequest $request)
    {
        $items = $this->items($request->get('type'), $request->get('items'))
            ->each(function ($item) {
                $item->removeBlacklist();
            })
            ->pluck('id')
            ->all();

        return response()->json($items);
    }

    /**
     * Returns a Collection of items based on the requested type
     *
     * @param string $type
     * @param array $items
     * @param boolean $withBlacklisted
     * @return Collection
     */
    private function items($type, $items = [], $withBlacklisted = false)
    {
        switch ($type) {
            case 'APPLICATION':
                $collection = Application::withBlacklisted()
                    ->whereIn('id', $items)
                    ->when($withBlacklisted, function ($query) {
                        return $query->withBlacklisted();
                    })
                    ->get();
                break;
            case 'CATEGORY':
                $collection = Category::withBlacklisted()
                    ->whereIn('id', $items)
                    ->when($withBlacklisted, function ($query) {
                        return $query->withBlacklisted();
                    })
                    ->get();
                break;
            case 'DEVELOPER':
                $collection = Developer::withBlacklisted()
                    ->whereIn('id', $items)
                    ->when($withBlacklisted, function ($query) {
                        return $query->withBlacklisted();
                    })
                    ->get();
                break;
            default:
                $collection = collect([]);
                break;
        }

        return $collection;
    }
}
