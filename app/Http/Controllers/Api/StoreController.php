<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreIndexRequest;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    /**
     * Returns a Collection of Applications
     *
     * @param ApplicationIndexRequest $request
     * @return Response
     */
    public function list(StoreIndexRequest $request)
    {
        $stores = Store::getStoreCategoryList();

        return response()->json(['data' => $stores]);
    }
}
