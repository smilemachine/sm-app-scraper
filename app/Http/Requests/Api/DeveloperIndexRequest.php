<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class DeveloperIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'blacklisted' => 'integer|min:0|max:2',
        ];

        $rulesWhenPresent = [
            'name' => 'string',
            'store' => 'integer|exists:stores,id',
        ];

        foreach ($rulesWhenPresent as $key => $value) {
            if ($this->get($key)) {
                $rules[$key] = $value;
            }
        }

        return $rules;
    }
}
