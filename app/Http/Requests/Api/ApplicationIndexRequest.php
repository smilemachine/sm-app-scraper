<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'priorityDirection' => 'string|in:LESS,MORE,NONE',
            'ratingDirection' => 'string|in:LESS,MORE',
            'updatedDirection' => 'string|in:LESS,MORE',
            'blacklisted' => 'integer|min:0|max:2',
            'trashed' => 'integer|min:0|max:2',
        ];

        $rulesWhenPresent = [
            'name' => 'string',
            'category' => 'integer|exists:categories,id',
            'priority' => 'integer|min:0|max:5',
            'rating' => 'numeric|min:0|max:5',
            'store' => 'integer|exists:stores,id',
            'developer' => 'integer|exists:developers,id',
            'updated' => 'date:YYYY-MM-DD'
        ];

        foreach ($rulesWhenPresent as $key => $value) {
            if ($this->get($key)) {
                $rules[$key] = $value;
            }
        }

        return $rules;
    }
}
