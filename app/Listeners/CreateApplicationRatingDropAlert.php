<?php

namespace App\Listeners;

use App\Models\Alert;
use App\Models\AlertType;
use App\Events\ApplicationRatingDrop;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateApplicationRatingDropAlert
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationRatingDrop  $event
     * @return void
     */
    public function handle(ApplicationRatingDrop $event)
    {
        $current = $event->getCurrent();
        $previous = $event->getPrevious();
        $alertType = AlertType::where('key', AlertType::RATING_DROP)->firstOrFail();

        if ($current && $previous) {
            $ratingType = 'total';
            $previousRating = $previous->total;
            $currentRating = $current->total;

            $description = 'Application #' . $current->application_id . ' has dropped in ' . $ratingType . ' ratings from ' . $previousRating . ' to ' . $currentRating . '.';

            $alert = Alert::firstOrNew([
                'alert_type_id' => $alertType->id,
                'application_id' => $current->application_id,
            ]);

            $alert->title = 'Application Rating Drop';
            $alert->description = $description;
            $alert->save();
        }
    }
}
