<?php

namespace App\Listeners;

use App\Models\Alert;
use App\Models\AlertType;
use App\Events\ApplicationRankingDrop;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateApplicationRankingDropAlert
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationRankingDrop  $event
     * @return void
     */
    public function handle(ApplicationRankingDrop $event)
    {
        $current = $event->getCurrent();
        $previous = $event->getPrevious();
        $alertType = AlertType::where('key', AlertType::RANKING_DROP)->firstOrFail();

        if ($current && $previous) {
            $previousRanking = $previous->ranking;
            $currentRanking = $current->ranking;

            $description = 'Application #' . $current->application_id . ' has dropped in ranking from ' . $previousRanking . ' to ' . $currentRanking . '.';

            $alert = Alert::firstOrNew([
                'alert_type_id' => $alertType->id,
                'application_id' => $current->application_id,
            ]);

            $alert->title = 'Application Ranking Drop';
            $alert->description = $description;
            $alert->save();
        }
    }
}
