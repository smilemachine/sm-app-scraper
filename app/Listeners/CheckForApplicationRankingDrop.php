<?php

namespace App\Listeners;

use App\Events\ApplicationRankingCreated;
use App\Events\ApplicationRankingDrop;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckForApplicationRankingDrop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationRankingCreated  $event
     * @return void
     */
    public function handle(ApplicationRankingCreated $event)
    {
        $current = $event->getCurrent();
        $previous = $event->getPrevious();

        if ($previous && $current->ranking > $previous->ranking) {
            event(new ApplicationRankingDrop($current, $previous));
        }
    }
}
