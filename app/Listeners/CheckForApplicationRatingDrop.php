<?php

namespace App\Listeners;

use App\Events\ApplicationRatingCreated;
use App\Events\ApplicationRatingDrop;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckForApplicationRatingDrop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationRatingCreated  $event
     * @return void
     */
    public function handle(ApplicationRatingCreated $event)
    {
        $current = $event->getCurrent();
        $previous = $event->getPrevious();

        if ($previous) {
            if ($current->total < $previous->total || $current->current < $previous->current) {
                event(new ApplicationRatingDrop($current, $previous));
            }
        }
    }
}
