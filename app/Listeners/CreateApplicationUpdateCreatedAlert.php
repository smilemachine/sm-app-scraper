<?php

namespace App\Listeners;

use App\Models\Alert;
use App\Models\AlertType;
use Carbon\Carbon;
use App\Events\ApplicationUpdateCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateApplicationUpdateCreatedAlert
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationNewUpdate  $event
     * @return void
     */
    public function handle(ApplicationUpdateCreated $event)
    {
        $current = $event->getCurrent();
        $previous = $event->getPrevious();
        $alertType = AlertType::where('key', AlertType::NEW_UPDATE)->firstOrFail();

        if ($previous) {
            $previousUpdate = $previous->created_at->format('Y-m-d');
            $currentUpdate = $current->created_at->format('Y-m-d');

            $alert = Alert::firstOrNew([
                'alert_type_id' => $alertType->id,
                'application_id' => $current->application_id,
            ]);

            $alert->title = 'Application Update';
            $alert->description = 'Application #' . $current->application_id . ' received a new update on ' . $currentUpdate . '. It was previously updated on ' . $previousUpdate . '.';
        }
    }
}
