<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ApplicationRatingCreated' => [
            'App\Listeners\CheckForApplicationRatingDrop',
        ],
        'App\Events\ApplicationRatingDrop' => [
            'App\Listeners\CreateApplicationRatingDropAlert',
        ],
        'App\Events\ApplicationRankingCreated' => [
            'App\Listeners\CheckForApplicationRankingDrop',
        ],
        'App\Events\ApplicationRankingDrop' => [
            'App\Listeners\CreateApplicationRankingDropAlert',
        ],
        'App\Events\ApplicationUpdateCreated' => [
            'App\Listeners\CreateApplicationUpdateCreatedAlert',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
