<?php

namespace App\Events;

use App\Models\ApplicationRating;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ApplicationRatingDrop
{
    use Dispatchable, SerializesModels;

    protected $current;
    protected $previous;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApplicationRating $current, ApplicationRating $previous)
    {
        $this->current = $current;
        $this->previous = $previous;
    }

    /**
     * Return the current ApplicationRating
     *
     * @return ApplicationRating
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Return the previous ApplicationRating
     *
     * @return ApplicationRating
     */
    public function getPrevious()
    {
        return $this->previous;
    }
}
