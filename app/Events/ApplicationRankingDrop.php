<?php

namespace App\Events;

use App\Models\ApplicationRanking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ApplicationRankingDrop
{
    use Dispatchable, SerializesModels;

    protected $current;
    protected $previous;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApplicationRanking $current, ApplicationRanking $previous)
    {
        $this->current = $current;
        $this->previous = $previous;
    }

    /**
     * Return the current ApplicationRanking
     *
     * @return ApplicationRanking
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Return the previous ApplicationRanking
     *
     * @return ApplicationRanking
     */
    public function getPrevious()
    {
        return $this->previous;
    }
}
