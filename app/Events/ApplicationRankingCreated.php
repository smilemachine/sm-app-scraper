<?php

namespace App\Events;

use App\Models\ApplicationRanking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ApplicationRankingCreated
{
    use Dispatchable, SerializesModels;

    protected $current;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApplicationRanking $current)
    {
        $this->current = $current;
    }

    /**
     * Return the ApplicationRanking
     *
     * @return ApplicationRating
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Returns the previous ApplicationRanking if applicable
     *
     * @return mixed
     */
    public function getPrevious()
    {
        return ApplicationRanking::where('id', '!=', $this->current->id)
            ->where('application_id', $this->current->application_id)
            ->where('country_id', $this->current->country_id)
            ->where('category_id', $this->current->category_id)
            ->orderBy('updated_at', 'DESC')
            ->first();
    }
}
