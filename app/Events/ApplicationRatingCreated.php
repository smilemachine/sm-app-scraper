<?php

namespace App\Events;

use App\Models\ApplicationRating;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ApplicationRatingCreated
{
    use Dispatchable, SerializesModels;

    protected $current;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApplicationRating $current)
    {
        $this->current = $current;
    }

    /**
     * Return the ApplicationRating
     *
     * @return ApplicationRating
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Returns the previous ApplicationRating if applicable
     *
     * @return mixed
     */
    public function getPrevious()
    {
        return ApplicationRating::where('id', '!=', $this->current->id)
            ->where('application_id', $this->current->application_id)
            ->where('country_id', $this->current->country_id)
            ->orderBy('updated_at', 'DESC')
            ->first();
    }
}
