<?php

namespace App\Events;

use App\Models\ApplicationUpdate;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ApplicationUpdateCreated
{
    use Dispatchable, SerializesModels;

    protected $current;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApplicationUpdate $current)
    {
        $this->current = $current;
    }

    /**
     * Return the ApplicationUpdate
     *
     * @return ApplicationUpdate
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Returns the previous ApplicationUpdate if applicable
     *
     * @return mixed
     */
    public function getPrevious()
    {
        return ApplicationUpdate::where('id', '!=', $this->current->id)
            ->where('application_id', $this->current->application_id)
            ->where('country_id', $this->current->country_id)
            ->orderBy('released_at', 'DESC')
            ->first();
    }
}
