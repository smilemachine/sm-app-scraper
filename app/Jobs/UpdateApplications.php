<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\ApplicationRanking;
use App\Models\Category;
use App\Models\Country;
use App\Models\Store;
use App\Services\AppScraper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class UpdateApplications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $category;
    protected $country;
    protected $start;
    protected $scraper;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Country $country, Category $category, $start = null)
    {
        $this->category = $category;
        $this->country = $country;
        $this->start = $start;
        $this->scraper = new AppScraper($this->category->store, $this->country);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $archive = $this->scraper->getApplications($this->category, null, $this->start);

        $this->processCurrentPage($archive);
    }

    /**
     * Process the results
     *
     * @param Collection $archive
     * @return void
     */
    private function processCurrentPage(Collection $archive)
    {
        if (isset($archive['results']) && is_array($archive['results'])) {
            collect($archive['results'])->each(function ($details, $key) {
                $application = Application::withTrashed()
                    ->withBlacklisted()
                    ->firstOrNew([
                        'app_id' => $details->id,
                    ]);

                // If this app has already been updated today, don't bother updating again
                if ($application->exists) {
                    $today = Carbon::now()->format('Ymd');

                    if ($application->updated_at->format('Ymd') >= $today) {
                        return;
                    }
                }

                $application = $this->scraper->fillApplicationFromSummary($application, $details);
                $application->store_id = $this->category->store->id;

                // Assign Developer
                $this->scraper->assignApplicationDeveloperFromDetails($application, $details);

                $application->save();

                // Assign the Category
                $this->assignApplicationCategories($application);

                // Assign the Ranking
                $ranking = intval($this->start + $key + 1);
                $this->assignApplicationRankings($application, $ranking);
            });

            // Log what we've done
            $startNumber = intval($this->start);
            $endNumber = $startNumber + count($archive['results']);

            logger()->info('Updated ' . $startNumber . '-' . $endNumber . ' of the top Applications: ' . $this->country->name . ', ' . $this->category->store->name . ', ' . $this->category->name);
        } else {
            logger()->debug('Results not found for ' . $this->category->store->name . ' summary list');
        }
    }

    /**
     * Assign the current Category to an Application
     *
     * @param Application $application
     * @return void
     */
    private function assignApplicationCategories(Application $application)
    {
        $attributes = [
            'application_id' => $application->id,
            'category_id' => $this->category->id,
        ];

        $application->applicationCategories()
            ->updateOrCreate($attributes, $attributes);
    }

    /**
     * Assign the current ranking to an Application
     *
     * @param Application $application
     * @param integer $ranking
     * @return void
     */
    private function assignApplicationRankings(Application $application, $ranking)
    {
        $latest = ApplicationRanking::where('application_id', $application->id)
            ->where('country_id', $this->country->id)
            ->where('category_id', $this->category->id)
            ->orderBy('updated_at', 'DESC')
            ->first();

        if ($latest && $latest->ranking == $ranking) {
            $latest->updated_at = Carbon::now();
            $latest->save();
        } else {
            ApplicationRanking::create([
                'application_id' => $application->id,
                'country_id' => $this->country->id,
                'category_id' => $this->category->id,
                'ranking' => $ranking
            ]);
        }
    }
}
