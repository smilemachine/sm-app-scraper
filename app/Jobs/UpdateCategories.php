<?php

namespace App\Jobs;

use App\Models\Category;
use App\Models\Country;
use App\Models\Store;
use App\Services\AppScraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCategories implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $store;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $store = $this->store;
        $country = Country::default()->firstOrFail();
        $scraper = new AppScraper($store, $country);
        $categories = $scraper->getCategories();
        $newKeys = $categories->values()->all();
        $existingKeys = Category::withTrashed()
            ->withBlacklisted()
            ->where('store_id', $store->id)
            ->get()
            ->pluck('key')
            ->all();

        // Blacklist any Categories that aren't in this list
        Category::where('store_id', $this->store->id)
            ->whereNotIn('key', $newKeys)
            ->each(function ($category) {
                $category->blacklist();
            });

        // Create any new Categories that don't yet exist
        $categories
            ->filter(function ($key, $name) use ($existingKeys) {
                return !in_array($key, $existingKeys);
            })
            ->each(function ($key, $name) use ($store) {
                $category = Category::create([
                    'store_id' => $store->id,
                    'key' => $key,
                    'name' => title_case(str_replace('_', ' ', $name))
                ]);
            });

        // Log the result
        logger()->info('Updated ' . count($categories) . ' Categories for ' . $store->name);
    }
}
