<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\ApplicationCategory;
use App\Models\Category;
use App\Models\Store;
use App\Services\AppScraper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class UpdateApplicationArchives implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $category;
    protected $letter;
    protected $page;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Category $category, $letter, $page)
    {
        $this->category = $category;
        $this->letter = $letter;
        $this->page = $page;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $scraper = new AppScraper($this->category->store);
        $archive = $scraper->getArchive($this->category, $this->letter, $this->page);

        $this->processCurrentPage($archive);
        $this->scheduleNextPage($archive);
    }

    /**
     * Process the results
     *
     * @param Collection $archive
     * @return void
     */
    private function processCurrentPage(Collection $archive)
    {
        if (isset($archive['results']) && is_array($archive['results'])) {
            collect($archive['results'])->each(function ($item) {
                $application = Application::firstOrNew([
                    'app_id' => $item->id,
                    'app_key' => $item->id,
                ]);

                $application->store_id = $this->category->store->id;
                $application->name = $item->title;
                $application->url = $item->url;
                $application->save();

                // Assign the Category
                $this->assignApplicationCategories($application);
            });

            logger()->info('Updated ' . count($archive['results']) . ' Applications for ' . $this->category->store->name . ' archive (Category: ' . $this->category->key . ', Letter: ' . $this->letter . ', Page: ' . $this->page . ')');
        } else {
            logger()->debug('Results not found for ' . $this->category->store->name . ' archive');
        }
    }

    /**
     * Add the next page to the Store's Queue
     *
     * @param Collection $archive
     * @return void
     */
    private function scheduleNextPage(Collection $archive)
    {
        if (isset($archive['next']) && !empty($archive['next'])) {
            $letter = null;
            $page = null;

            parse_str(trim($archive['next'], '\"'), $urlParams);

            if (isset($urlParams['letter']) && !empty($urlParams['letter'])) {
                $letter = strtoupper($urlParams['letter']);
            }

            if (isset($urlParams['page']) && intval($urlParams['page']) != 0) {
                $page = intval($urlParams['page']);
            }

            if ($letter && $page) {
                // Schedule it to run once every 20 seconds
                $delay = Carbon::now()
                    ->addSeconds((intval($page) + 1) * 20);

                $job = (new UpdateStoreApplicationArchive($this->category, $letter, $page))
                    ->onQueue($this->category->store->key)
                    ->delay($delay);

                dispatch($job);
            }
        }
    }

    /**
     * Assign the current Category to an Application
     *
     * @param Application $application
     * @return void
     */
    private function assignApplicationCategories(Application $application)
    {
        $attributes = [
            'application_id' => $application->id,
            'category_id' => $this->category->id,
        ];

        $application->applicationCategories()
            ->updateOrCreate($attributes, $attributes);
    }
}
