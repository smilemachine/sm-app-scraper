<?php

namespace App\Jobs;

use App\Models\Collection;
use App\Models\Country;
use App\Models\Store;
use App\Services\AppScraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCollections implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $store;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $store = $this->store;
        $country = Country::default()->firstOrFail();
        $scraper = new AppScraper($store, $country);
        $collections = $scraper->getCollections();
        $newKeys = $collections->values()->all();
        $existingKeys = Collection::withTrashed()
            ->where('store_id', $store->id)
            ->get()
            ->pluck('key')
            ->all();

        // Delete any Collections that aren't in this list
        Collection::where('store_id', $this->store->id)
            ->whereNotIn('key', $newKeys)
            ->delete();

        // Create any new Collections that don't yet exist
        $collections
            ->filter(function ($key, $name) use ($existingKeys) {
                return !in_array($key, $existingKeys);
            })
            ->each(function ($key, $name) use ($store) {
                $category = Collections::create([
                    'store_id' => $store->id,
                    'key' => $key,
                    'name' => title_case(str_replace('_', ' ', $name))
                ]);
            });

        // Log the result
        logger()->info('Updated ' . count($collections) . ' Collections for ' . $store->name);
    }
}
