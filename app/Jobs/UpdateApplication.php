<?php

namespace App\Jobs;

use App\Models\Application;
use App\Models\Country;
use App\Services\AppScraper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateApplication implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $application;
    protected $country;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Application $application, Country $country)
    {
        $this->application = $application;
        $this->country = $country;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $scraper = new AppScraper($this->application->store, $this->country);
        $details = $scraper->getApplication($this->application);

        if (!is_null($details) && $details) {
            $this->application = $scraper->fillApplicationFromDetails($this->application, $details);
            $this->application->save();

            $scraper->assignApplicationCategoriesFromDetails($this->application, $details);
            $scraper->assignApplicationDeveloperFromDetails($this->application, $details);
            $scraper->assignApplicationLanguagesFromDetails($this->application, $details);
            $scraper->assignApplicationRatingsFromDetails($this->application, $details);
            $scraper->assignApplicationReviewsFromDetails($this->application, $details);
            $scraper->assignApplicationUpdatesFromDetails($this->application, $details);

            logger()->info('Updated Application #' . $this->application->id . ' from ' . $this->application->store->name . ' (' . $this->country->name . ')');
        } else {
            $this->application->delete();
            logger()->info('Deleted Application #' . $this->application->id . ' from ' . ($this->application->store ? $this->application->store->name : 'scraper'));
        }
    }
}
