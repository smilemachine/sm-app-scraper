<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateAlertTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('alert_types')->insert([
            ['key'=>'rating_drop', 'name'=>'Rating Drop'],
            ['key'=>'new_bad_reviews', 'name'=>'New Bad Reviews'],
            ['key'=>'new_update', 'name'=>'New Update'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DELETE FROM `alert_types`');
    }
}
