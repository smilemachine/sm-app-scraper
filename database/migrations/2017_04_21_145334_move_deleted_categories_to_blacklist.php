<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveDeletedCategoriesToBlacklist extends Migration
{
    private $categories = ["6000", "6015", "6013", "6020", "6003", "BUSINESS", "FINANCE", "HEALTH_AND_FITNESS", "MEDICAL", "TRAVEL_AND_LOCAL"];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('categories')
            ->update([
                'deleted_at' => null,
            ]);

        $categories = DB::table('categories')
            ->whereNotIn('key', $this->categories)
            ->get();

        $categories->each(function ($category) {
            DB::table('blacklists')->insert([
                'blacklistable_id' => $category->id,
                'blacklistable_type' => 'App\Models\Category',
                'reason' => 'Moving trashed Categories to blacklisted.',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('blacklists')
            ->where('blacklistable_type', 'like', '%Category')
            ->delete();

        DB::table('categories')
            ->whereNotIn('key', $this->categories)
            ->update([
                'deleted_at' => Carbon::now()->toDateTimeString()
            ]);
    }
}
