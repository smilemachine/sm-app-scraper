<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->decimal('total', 3, 1)->nullable();
            $table->decimal('current', 3, 1)->nullable();
            $table->integer('1star')->nullable();
            $table->integer('2star')->nullable();
            $table->integer('3star')->nullable();
            $table->integer('4star')->nullable();
            $table->integer('5star')->nullable();
            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')->on('applications')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_ratings');
    }
}
