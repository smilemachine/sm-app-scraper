<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRatingsReviewsFromApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applications', function (Blueprint $table) {
            $table->dropColumn('rating_calculated_total');
            $table->dropColumn('rating_calculated_current');
            $table->dropColumn('rating_calculated_1star');
            $table->dropColumn('rating_calculated_2star');
            $table->dropColumn('rating_calculated_3star');
            $table->dropColumn('rating_calculated_4star');
            $table->dropColumn('rating_calculated_5star');
            $table->dropColumn('reviews_calculated_total');
            $table->dropColumn('reviews_calculated_version');
            $table->dropColumn('changes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applications', function (Blueprint $table) {
            $table->decimal('rating_calculated_total', 3, 1)->nullable()->after('currency');
            $table->decimal('rating_calculated_current', 3, 1)->nullable()->after('rating_calculated_total');
            $table->integer('rating_calculated_1star')->nullable()->after('rating_calculated_current');
            $table->integer('rating_calculated_2star')->nullable()->after('rating_calculated_1star');
            $table->integer('rating_calculated_3star')->nullable()->after('rating_calculated_2star');
            $table->integer('rating_calculated_4star')->nullable()->after('rating_calculated_3star');
            $table->integer('rating_calculated_5star')->nullable()->after('rating_calculated_4star');
            $table->integer('reviews_calculated_total')->nullable()->after('rating_calculated_5star');
            $table->integer('reviews_calculated_version')->nullable()->after('reviews_calculated_total');
            $table->text('changes')->nullable()->after('app_updated_at');
        });
    }
}
