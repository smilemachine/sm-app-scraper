<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeChangesFieldType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applications', function (Blueprint $table) {
            $table->text('changes')->nullable()->change();
        });

        Schema::table('application_histories', function (Blueprint $table) {
            $table->text('changes')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('UPDATE `applications` SET `changes`=SUBSTR(`changes`, 1, 255)');
        \DB::statement('UPDATE `application_histories` SET `changes`=SUBSTR(`changes`, 1, 255)');

        Schema::table('applications', function (Blueprint $table) {
            $table->string('changes')->nullable()->change();
        });

        Schema::table('application_histories', function (Blueprint $table) {
            $table->string('changes')->nullable()->change();
        });
    }
}
