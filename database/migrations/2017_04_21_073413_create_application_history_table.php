<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->string('version')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price', 6, 2)->nullable();
            $table->string('currency')->nullable();
            $table->decimal('rating_calculated_total', 3, 1)->nullable();
            $table->decimal('rating_calculated_current', 3, 1)->nullable();
            $table->integer('rating_calculated_1star')->nullable();
            $table->integer('rating_calculated_2star')->nullable();
            $table->integer('rating_calculated_3star')->nullable();
            $table->integer('rating_calculated_4star')->nullable();
            $table->integer('rating_calculated_5star')->nullable();
            $table->integer('reviews_calculated_total')->nullable();
            $table->integer('reviews_calculated_version')->nullable();
            $table->integer('installs_min')->nullable();
            $table->integer('installs_max')->nullable();
            $table->string('content_rating')->nullable();
            $table->timestamp('app_released_at')->nullable();
            $table->timestamp('app_updated_at')->nullable();
            $table->string('changes')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')->on('applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_histories');
    }
}
