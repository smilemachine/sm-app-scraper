<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alert_type_id')->unsigned();
            $table->integer('application_id')->unsigned();
            $table->string('title');
            $table->string('description')->nullable();
            $table->boolean('is_read')->default(0);
            $table->boolean('is_actioned')->default(0);
            $table->timestamps();

            $table->foreign('alert_type_id')
                ->references('id')->on('alert_types')
                ->onDelete('cascade');

            $table->foreign('application_id')
                ->references('id')->on('applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
