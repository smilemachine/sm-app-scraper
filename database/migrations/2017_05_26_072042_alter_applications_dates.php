<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterApplicationsDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applications', function (Blueprint $table) {
            $table->dropColumn('app_released_at');
            $table->dropColumn('app_updated_at');
            $table->timestamp('released_at')->nullable()->after('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applications', function (Blueprint $table) {
            $table->dropColumn('released_at');
            $table->timestamp('app_released_at')->after('content_rating')->nullable();
            $table->timestamp('app_updated_at')->after('app_released_at')->nullable();
        });
    }
}
