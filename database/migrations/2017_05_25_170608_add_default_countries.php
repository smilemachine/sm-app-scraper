<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('countries')->insert([
            ['key'=>'au', 'name'=>'Australia'],
            ['key'=>'ca', 'name'=>'Canada'],
            ['key'=>'fr', 'name'=>'France'],
            ['key'=>'de', 'name'=>'Germany'],
            ['key'=>'gb', 'name'=>'Great Britain'],
            ['key'=>'it', 'name'=>'Italy'],
            ['key'=>'es', 'name'=>'Spain'],
            ['key'=>'us', 'name'=>'USA'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('DELETE FROM `countries`');
    }
}
