<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_id');
            $table->string('app_key');
            $table->string('name');
            $table->string('url');
            $table->string('version')->nullable();
            $table->integer('developer_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->decimal('price', 6, 2)->nullable();
            $table->string('currency')->nullable();
            $table->decimal('rating_calculated_total', 3, 1)->nullable();
            $table->decimal('rating_calculated_current', 3, 1)->nullable();
            $table->integer('rating_calculated_1star')->nullable();
            $table->integer('rating_calculated_2star')->nullable();
            $table->integer('rating_calculated_3star')->nullable();
            $table->integer('rating_calculated_4star')->nullable();
            $table->integer('rating_calculated_5star')->nullable();
            $table->integer('reviews_calculated_total')->nullable();
            $table->integer('reviews_calculated_version')->nullable();
            $table->integer('installs_min')->nullable();
            $table->integer('installs_max')->nullable();
            $table->string('content_rating')->nullable();
            $table->timestamp('app_released_at')->nullable();
            $table->timestamp('app_updated_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['developer_id', 'app_id']);

            $table->foreign('developer_id')
                ->references('id')->on('developers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
