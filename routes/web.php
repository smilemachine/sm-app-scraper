<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')
        ->name('dashboard');

    Route::group(['prefix' => 'alerts'], function () {
        Route::get('/', 'AlertController@index')
            ->name('alert.index');
    });

    Route::group(['prefix' => 'applications'], function () {
        Route::get('/', 'ApplicationController@index')
            ->name('application.index');
        Route::get('{id}', 'ApplicationController@show')
            ->name('application.show');
        Route::patch('{id}', 'ApplicationController@update')
            ->name('application.update');
        Route::get('{id}/blacklist/add', 'ApplicationController@addBlacklist')
            ->name('application.blacklist.add');
        Route::get('{id}/blacklist/remove', 'ApplicationController@removeBlacklist')
            ->name('application.blacklist.remove');
        Route::get('{id}/restore', 'ApplicationController@restore')
            ->name('application.restore');
        Route::get('{id}/delete', 'ApplicationController@delete')
            ->name('application.delete');
    });

    Route::group(['prefix' => 'developers'], function () {
        Route::get('/', 'DeveloperController@index')
            ->name('developer.index');
        Route::get('{id}', 'DeveloperController@show')
            ->name('developer.show');
        Route::get('{id}/blacklist/add', 'DeveloperController@addBlacklist')
            ->name('developer.blacklist.add');
        Route::get('{id}/blacklist/remove', 'DeveloperController@removeBlacklist')
            ->name('developer.blacklist.remove');
        Route::get('{id}/restore', 'DeveloperController@restore')
            ->name('developer.restore');
        Route::get('{id}/delete', 'DeveloperController@delete')
            ->name('developer.delete');
    });

    Route::group(['prefix' => 'scheduler'], function () {
        Route::get('/', 'SchedulerController@index')
            ->name('scheduler.index');
        Route::post('/', 'SchedulerController@schedule')
            ->name('scheduler.schedule');
    });
});
