<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'alerts'], function () {
        Route::get('/', 'AlertController@index');
        Route::post('actioned', 'AlertController@actioned');
    });

    Route::group(['prefix' => 'alert-types'], function () {
        Route::get('list', 'AlertTypeController@list');
    });

    Route::group(['prefix' => 'applications'], function () {
        Route::get('/', 'ApplicationController@index');
        Route::post('priority', 'ApplicationController@updatePriority');
        Route::get('priorities', 'ApplicationController@priorityList');
    });

    Route::group(['prefix' => 'developers'], function () {
        Route::get('/', 'DeveloperController@index');
    });

    Route::group(['prefix' => 'stores'], function () {
        Route::get('list', 'StoreController@list');
    });

    Route::group(['prefix' => 'blacklist'], function () {
        Route::post('add', 'BlacklistController@add');
        Route::post('remove', 'BlacklistController@remove');
    });

    Route::group(['prefix' => 'reports'], function () {
        Route::get('updated-vs-rating', 'ReportController@updatedVsRating');
    });
});
